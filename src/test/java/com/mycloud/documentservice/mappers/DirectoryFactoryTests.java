package com.mycloud.documentservice.mappers;

import com.mycloud.documentservice.dto.DirectoryDto;
import com.mycloud.documentservice.dto.factory.DirectoryFactory;
import com.mycloud.documentservice.entities.Directory;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class DirectoryFactoryTests {
	@Test
	public void testFactoryReturnsDirectoryDtoNoSubdirectories() {
		Directory directory = new Directory();
		UUID id = UUID.randomUUID();
		Set<Directory> subDirectories = Collections.emptySet();

		directory.setId(id);
		directory.setName("somename");
		directory.setPath("somepath");
		directory.setSubDirectories(subDirectories);

		DirectoryDto expectedDirectoryDto = new DirectoryDto(id, "somename", "somepath", Collections.emptyList());

		DirectoryDto actualDirectoryDto = DirectoryFactory.directoryDto(directory);

		assertEquals(expectedDirectoryDto.getId(), actualDirectoryDto.getId());
		assertEquals(expectedDirectoryDto.getName(), actualDirectoryDto.getName());
		assertEquals(expectedDirectoryDto.getPath(), actualDirectoryDto.getPath());
		assertEquals(expectedDirectoryDto.getSubdirectories().size(), 0);
	}

	@Test
	public void testFactoryReturnsDirectoryDtoWithSubdirectories() {
		Directory directory = new Directory();
		UUID id = UUID.randomUUID();

		Directory subDirectory = new Directory();
		subDirectory.setId(id);
		subDirectory.setName("somename");
		subDirectory.setPath("somepath");
		subDirectory.setSubDirectories(Collections.emptySet());

		Set<Directory> subDirectories = Set.of(subDirectory);

		directory.setId(id);
		directory.setName("somename");
		directory.setPath("somepath");
		directory.setSubDirectories(subDirectories);

		DirectoryDto subdirectoryDto = DirectoryFactory.directoryDto(subDirectory);
		DirectoryDto expectedDirectoryDto = new DirectoryDto(id, "somename", "somepath", List.of(subdirectoryDto));

		DirectoryDto actualDirectoryDto = DirectoryFactory.directoryDto(directory);

		assertEquals(expectedDirectoryDto.getId(), actualDirectoryDto.getId());
		assertEquals(expectedDirectoryDto.getName(), actualDirectoryDto.getName());
		assertEquals(expectedDirectoryDto.getPath(), actualDirectoryDto.getPath());
		assertEquals(expectedDirectoryDto.getSubdirectories().size(), 1);

		assertEquals(expectedDirectoryDto.getSubdirectories().get(0).getId(), subDirectory.getId());
	}
}
