package com.mycloud.documentservice.integration;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Map;

@ContextConfiguration(initializers = AbstractTestCase.Initializer.class)
@Testcontainers
@ActiveProfiles("test")
public class AbstractTestCase {

	private static final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:12.6")
			.withUsername("admin")
			.withPassword("admin");

	static {
		postgreSQLContainer.start();
	}


	private static Map<String, String> createConnectionConfiguration() {
		return Map.of(
				"spring.datasource.url", postgreSQLContainer.getJdbcUrl(),
				"spring.datasource.username", postgreSQLContainer.getUsername(),
				"spring.datasource.password", postgreSQLContainer.getPassword()
		);
	}

	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

		@Override
		public void initialize(ConfigurableApplicationContext applicationContext) {

			ConfigurableEnvironment environment =
					applicationContext.getEnvironment();

			MapPropertySource testcontainers = new MapPropertySource(
					"testcontainers",
					(Map) createConnectionConfiguration()
			);

			environment.getPropertySources().addFirst(testcontainers);
		}
	}
}
