package com.mycloud.documentservice.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycloud.documentservice.DocumentserviceApplication;
import com.mycloud.documentservice.dto.MoveEntitiesRequest;
import com.mycloud.documentservice.dto.MoveEntityDto;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.EntityType;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.util.FileSystemUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = DocumentserviceApplication.class)
@AutoConfigureMockMvc
public class DocumentTests extends AbstractTestCase {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private DirectoryRepository directoryRepository;
	@Autowired
	private DocumentRepository documentRepository;

	private final Path basePath = Paths.get(System.getProperty("user.home"), ".mycloud");

	@Test
	public void moveDocumentToPath() throws Exception {
		String userId = "1000";
		String rootDirectoryPath = String.format("%s/%s", basePath, userId);
		String sourceDirectoryPath = String.format("%s/%s/%s", basePath, userId, "tmp");
		String destinationDirectoryPath = String.format("%s/%s/%s", basePath, userId, "home");
		String documentPath = String.format("%s/%s/%s/%s", basePath, userId, "tmp", "item.png");


		Files.createDirectories(Path.of(rootDirectoryPath));
		Files.createDirectories(Path.of(sourceDirectoryPath));
		Files.createDirectories(Path.of(destinationDirectoryPath));

		Files.createFile(Path.of(documentPath));

		Directory root = new Directory();
		root.setId(UUID.randomUUID());
		root.setPath(rootDirectoryPath);
		root.setName("root");

		Directory destination = new Directory();
		destination.setId(UUID.randomUUID());
		destination.setPath(destinationDirectoryPath);
		destination.setName("home");
		destination.setParentDir(root);

		Directory sourceDirectory = new Directory();
		sourceDirectory.setId(UUID.randomUUID());
		sourceDirectory.setPath(sourceDirectoryPath);
		sourceDirectory.setName("tmp");
		sourceDirectory.setParentDir(root);


		Document document = new Document();
		document.setId(UUID.randomUUID());
		document.setPath(documentPath);
		document.setName("item.png");
		document.setUploadedAt(LocalDateTime.now());

		directoryRepository.saveAll(List.of(root, destination, sourceDirectory));
		documentRepository.save(document);


		MoveEntitiesRequest request = new MoveEntitiesRequest();

		request.setDestinationDirectoryId(destination.getId());
		request.setItems(List.of(new MoveEntityDto(document.getId(), EntityType.DOCUMENT)));

		RequestBuilder builder = post("/documents/move")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request));

		String expectedResponse = "{'moved': true}";


		mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(content().json(expectedResponse))
				.andReturn();

		String expectedNewPathOfDocument = String.format("%s/%s/%s", basePath, userId, "home/item.png");

		assertTrue(Files.exists(Path.of(expectedNewPathOfDocument)));
		assertFalse(Files.exists(Path.of(documentPath)));

		assertEquals(expectedNewPathOfDocument, documentRepository.findById(document.getId()).get().getPath());


		FileSystemUtils.deleteRecursively(Path.of(rootDirectoryPath));
	}

	@Test
	public void moveDirectoryWithFiles() throws Exception {
		String userId = "5000";
		String rootDirectoryPath = String.format("%s/%s", basePath, userId);
		String directoryToMovePath = String.format("%s/%s/%s", basePath, userId, "tmp");
		String destinationDirectoryPath = String.format("%s/%s/%s", basePath, userId, "home");
		String documentPath = String.format("%s/%s/%s/%s", basePath, userId, "tmp", "item1.png");

		Directory root = new Directory();
		root.setId(UUID.randomUUID());
		root.setPath(rootDirectoryPath);
		root.setName("root");

		Directory destination = new Directory();
		destination.setId(UUID.randomUUID());
		destination.setPath(destinationDirectoryPath);
		destination.setParentDir(root);
		destination.setName("home");

		Directory sourceDirectory = new Directory();
		sourceDirectory.setId(UUID.randomUUID());
		sourceDirectory.setPath(directoryToMovePath);
		sourceDirectory.setParentDir(root);
		sourceDirectory.setName("tmp");

		Document document = new Document();
		document.setId(UUID.randomUUID());
		document.setPath(documentPath);
		document.setName("item.png");
		document.setUploadedAt(LocalDateTime.now());
		document.setDirectory(sourceDirectory);

		directoryRepository.saveAll(List.of(root, destination, sourceDirectory));
		documentRepository.save(document);

		Files.createDirectories(Path.of(rootDirectoryPath));
		Files.createDirectories(Path.of(directoryToMovePath));
		Files.createDirectories(Path.of(destinationDirectoryPath));
		Files.createFile(Path.of(documentPath));

		MoveEntitiesRequest request = new MoveEntitiesRequest();

		request.setDestinationDirectoryId(destination.getId());
		request.setItems(List.of(new MoveEntityDto(sourceDirectory.getId(), EntityType.DIRECTORY)));

		RequestBuilder builder = post("/documents/move")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request));

		String expectedResponse = "{'moved': true}";

		mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(content().json(expectedResponse))
				.andReturn();

		String expectedNewPathOfDirectory = String.format("%s/%s/%s/%s", basePath, userId, "home", "tmp");
		String expectedNewPathOfDocumentInMovedDirectory = String.format("%s/%s/%s/%s/%s", basePath, userId, "home", "tmp", "item1.png");

		assertTrue(Files.exists(Path.of(expectedNewPathOfDirectory)));
		assertTrue(Files.exists(Path.of(expectedNewPathOfDocumentInMovedDirectory)));

		assertEquals(expectedNewPathOfDirectory, directoryRepository.findById(sourceDirectory.getId()).get().getPath());

		FileSystemUtils.deleteRecursively(Path.of(rootDirectoryPath));
	}

	@Test
	public void testMoveNonExistingDestinationDirectory() throws Exception {
		MoveEntitiesRequest request = new MoveEntitiesRequest();
		UUID randomUUID = UUID.randomUUID();

		request.setDestinationDirectoryId(randomUUID);
		request.setItems(List.of(new MoveEntityDto(randomUUID, EntityType.DOCUMENT)));

		RequestBuilder builder = post("/documents/move")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request));

		String expectedResponse = "{'error': 'destination directory not found'}";

		mockMvc.perform(builder)
				.andExpect(status().isNotFound())
				.andExpect(content().json(expectedResponse))
				.andReturn();
	}


	@Test
	public void testGetLatestDocumentsWithLimitNonMatchingUserId() throws Exception {
		RequestBuilder builder = get("/documents/1500/latest?limit=5")
				.contentType(MediaType.APPLICATION_JSON);

		String expectedResponse = "{'result': []}";

		mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(content().json(expectedResponse))
				.andExpect(jsonPath("$.result", hasSize(0)))
				.andReturn();
	}

	@Test
	public void testGetLatestDocumentsWithLimitMatchingUserId() throws Exception {

		Directory directory = new Directory();

		directory.setId(UUID.randomUUID());
		directory.setPath("somepath");
		directory.setName("somename");
		directory.setUserId(100);

		directoryRepository.save(directory);

		Document document = new Document.Builder()
				.setName("somename.png")
				.setPath(Path.of("randomPath"))
				.setUploadedAt(LocalDateTime.now())
				.setDirectory(directory)
				.build();

		documentRepository.save(document);

		RequestBuilder builder = get("/documents/100/latest?limit=1")
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.result", hasSize(1)))
				.andReturn();
	}

	@Test
	public void testGetLatestDocumentsMissingLimit() throws Exception {
		RequestBuilder builder = get("/documents/100/latest")
				.contentType(MediaType.APPLICATION_JSON);

		mockMvc.perform(builder)
				.andExpect(status().isBadRequest())
				.andReturn();
	}
}
