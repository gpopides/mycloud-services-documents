package com.mycloud.documentservice.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycloud.documentservice.DocumentserviceApplication;
import com.mycloud.documentservice.dto.CreateDirectoryDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@WebAppConfiguration
@SpringBootTest(classes = DocumentserviceApplication.class)
@AutoConfigureMockMvc
public class DirectoryTests extends AbstractTestCase {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	private final Path basePath = Paths.get(System.getProperty("user.home"), ".mycloud");

	@Test
	public void getFilesystemOfNewUserReturnsRootDirectoryDoesNotExist() throws Exception {
		RequestBuilder builder = get("/directories/fs")
				.param("userId", "1");

		String expectedResponse = "{'error': 'Root directory of user does not exist'}";

		mockMvc.perform(builder)
				.andExpect(status().isNotFound())
				.andExpect(content().json(expectedResponse))
				.andReturn();
	}

	@Test
	public void createDirectoryMissingPath() throws Exception {
		CreateDirectoryDto dto = new CreateDirectoryDto();

		RequestBuilder builder = post("/directories")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dto));

		mockMvc.perform(builder)
				.andExpect(status().isBadRequest());
	}


	@Test
	public void createDirectorySuccessful() throws Exception {
		int userID = 1500;
		String newDirectoryPath = "testPath";
		CreateDirectoryDto dto = new CreateDirectoryDto(newDirectoryPath, userID);

		RequestBuilder builder = post("/directories")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(dto));

		Path expectedPath = Paths.get(basePath.toString(), String.valueOf(userID), newDirectoryPath);
		String expectedResponse = "{'created': true}";

		mockMvc.perform(builder)
				.andExpect(status().isOk())
				.andExpect(content().json(expectedResponse));

		assertTrue(Files.exists(expectedPath));

		//cleanup
		Files.delete(expectedPath);
		Files.delete(expectedPath.getParent());
	}
}
