package com.mycloud.documentservice.integration;

import com.mycloud.documentservice.DocumentserviceApplication;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.repositories.FilesystemRedisService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.nio.file.Path;
import java.time.LocalDateTime;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = DocumentserviceApplication.class)
@AutoConfigureMockMvc
public class FilesystemTests extends AbstractTestCase {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DirectoryRepository directoryRepository;
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private FilesystemRedisService filesystemRedisService;

    @AfterEach
    public void tearDown() {
        filesystemRedisService.deleteSynchronizationTimestamp("testClient");
    }


    @Test
    public void testSynchronizeFirstTimeReturnsAllStoredItems() throws Exception {
        int userId = 5;
        Directory directory = new Directory("sometpath", "somename", userId);
        directoryRepository.save(directory);

        Document document = new Document.Builder()
                .setName("someName.png")
                .setPath(Path.of("somepath"))
                .setUploadedAt(LocalDateTime.now())
                .setDirectory(directory)
                .build();


        documentRepository.save(document);

        String url = "/filesystem/" + userId + "/synchronize?cid=testClient";

        RequestBuilder builder = get(url)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("results.documentIds", hasSize(1)))
                .andExpect(jsonPath("results.directoryIds", hasSize(1)))
                .andExpect(jsonPath("results.directoryIds", contains(directory.getId().toString())))
                .andExpect(jsonPath("results.documentIds", contains(document.getId().toString())))
                .andReturn();

    }

    @Test
    public void testSynchronizeWithExistingTimestampReturnsNoNewItems() throws Exception {
        int userId = 5;
        Directory directory = new Directory("sometpath", "somename", userId);
        directoryRepository.save(directory);

        Document document = new Document.Builder()
                .setName("someName.png")
                .setPath(Path.of("somepath"))
                .setUploadedAt(LocalDateTime.now())
                .setDirectory(directory)
                .build();

        documentRepository.save(document);

        String url = "/filesystem/" + userId + "/synchronize?cid=testClient";

        RequestBuilder builder = get(url)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(builder)
                .andReturn();

        mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("results.documentIds", hasSize(0)))
                .andExpect(jsonPath("results.directoryIds", hasSize(0)))
                .andReturn();

        filesystemRedisService.deleteSynchronizationTimestamp("testClient");
    }

    @Test
    public void testSynchronizeMissingClientIsBadRequest() throws Exception {
        int userId = 5;

        String url = "/filesystem/" + userId + "/synchronize";

        RequestBuilder builder = get(url)
                .contentType(MediaType.APPLICATION_JSON);


        mockMvc.perform(builder)
                .andExpect(status().isBadRequest())
                .andReturn();

    }
}
