package com.mycloud.documentservice.unit;

import com.mycloud.documentservice.dto.SendMethod;
import com.mycloud.documentservice.dto.ShareReceiver;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ShareDocumentTests {

	@Test
	public void test_receiverInfoIsValidEmailExists() {
		ShareReceiver receiver = new ShareReceiver();
		receiver.setEmail("someemail");
		assertTrue(receiver.receiverInfoIsValid(SendMethod.EMAIL));
	}

	@Test
	public void test_receiverInfoIsValidMissingEmail() {
		ShareReceiver receiver = new ShareReceiver();
		receiver.setPhone("12389127831");
		assertFalse(receiver.receiverInfoIsValid(SendMethod.EMAIL));
	}

	@Test
	public void test_receiverInfoIsValidPhoneExists() {
		ShareReceiver receiver = new ShareReceiver();
		receiver.setPhone("12389127831");
		assertTrue(receiver.receiverInfoIsValid(SendMethod.PHONE));
	}
}
