package com.mycloud.documentservice.unit;

import com.mycloud.documentservice.beans.FileManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class FileManagerTests {
	private final Path basePath = Paths.get(System.getProperty("user.home"), ".mycloud");

	@Test
	public void getDestinationPath() {
		String p1 = "/some/path/";
		String p2 = "/some/other/path/filename.png";

		Path expected = Path.of("/some/path/filename.png");

		assertEquals(expected, FileManager.getDestinationFullPath(p2, p1));
	}

	@Test
	public void testMoveFromTo() throws IOException {
		String p1 = String.format("%s/%s", basePath, "some/path/");
		String p2 = String.format("%s/%s", basePath, "some/other/path");

		String filename = "item.png";

		Files.createDirectories(Path.of(p1));
		Files.createDirectories(Path.of(p2));


		String source = String.format("%s/%s", p2, filename);

		Files.createFile(Path.of(source));

		FileManager.moveTo(source, p1);

		String expectedNewPath = String.format("%s/%s", p1, filename);

		assertTrue(Files.exists(Path.of(expectedNewPath)));
		assertFalse(Files.exists(Path.of(source)));

		Files.delete(Path.of(expectedNewPath));
		Files.delete(Path.of(p1));
		Files.delete(Path.of(p2));
		Files.delete(Path.of(String.format("%s/%s/%s", basePath, "some", "other")));
		Files.delete(Path.of(String.format("%s/%s", basePath, "some")));
	}
}
