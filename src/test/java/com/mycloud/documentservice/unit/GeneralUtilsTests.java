package com.mycloud.documentservice.unit;

import com.mycloud.documentservice.exceptions.InvalidUploadException;
import com.mycloud.documentservice.utils.GeneralUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GeneralUtilsTests {
	@Test
	public void testBytesToHumanFormatReturns0() {
		assertEquals("0", GeneralUtils.bytesToHumanFormat(0));
	}

	@Test
	public void testBytesToHumanFormatReturns1K() {
		assertEquals("1 KB", GeneralUtils.bytesToHumanFormat(1024));
	}

	@Test
	public void testBytesToHumanFormatReturns1M() {
		assertEquals("1 MB", GeneralUtils.bytesToHumanFormat(1024 * 1024));
	}

	@Test
	public void testBytesToHumanFormatReturns2M() {
		assertEquals("2 MB", GeneralUtils.bytesToHumanFormat(1024 * 1024 * 2));
	}

	@Test
	public void testBytesToHumanFormatReturns1G() {
		assertEquals("1 GB", GeneralUtils.bytesToHumanFormat(1024 * 1024 * 1024));
	}

	@Test
	public void testGetFileChunkSequenceIndexReturns1() {
		String fname = "someid_1";
		assertEquals(1, GeneralUtils.getFileChunkSequenceIndex(fname));
	}

	@Test
	public void testGetFileChunkSequenceIndexThrowsExceptionInvalidIdMissingUnderscore() {
		String fname = "someinvalidId";

		assertThrows(InvalidUploadException.class, () -> GeneralUtils.getFileChunkSequenceIndex(fname));
	}

	@Test
	public void testGenerateRandomStringReturnsCorrectStringLength() {
		int length = 5;
		String randomString = GeneralUtils.generateRandomString(length);
		assertEquals(5, randomString.length());
	}

	@Test
	public void testGenerateRandomStringContainsOnlyCharsAndDigits() {
		int length = 5;
		String randomString = GeneralUtils.generateRandomString(length);
		for (int i = 0; i < randomString.length(); ++i) {
			int asciiValue = randomString.charAt(i);
			assertTrue(asciiValue >= 48 && asciiValue <= 122);
		}
	}
}
