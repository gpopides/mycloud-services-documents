package com.mycloud.documentservice.serviceunit;

import com.mycloud.documentservice.dto.SynchronizeResultDto;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.repositories.FilesystemRedisService;
import com.mycloud.documentservice.services.FilesystemService;
import com.mycloud.documentservice.services.FilesystemServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class FilesystemServiceTests {
    private final DirectoryRepository directoryRepository = mock(DirectoryRepository.class);
    private final DocumentRepository documentRepository = mock(DocumentRepository.class);
    private final FilesystemRedisService filesystemRedisService = mock(FilesystemRedisService.class);

    @InjectMocks
    private final FilesystemService filesystemService = new FilesystemServiceImpl(
            directoryRepository,
            documentRepository,
            filesystemRedisService
    );

    @Test
    public void testSynchronizeWithNonExistingClientId() {
        String clientID = "someid";
        int userID = 5;
        LocalDateTime newSyncTimestamp = mock(LocalDateTime.class);
        Optional<LocalDateTime> lastSyncTimestamp = Optional.empty();

        Document storedDocument = new Document();
        storedDocument.setId(UUID.randomUUID());

        Directory storedDirectory = new Directory();
        storedDirectory.setId(UUID.randomUUID());

        SynchronizeResultDto expectedResult = new SynchronizeResultDto();
        expectedResult.setDirectoryIds(List.of(storedDirectory.getId()));
        expectedResult.setDocumentIds(List.of(storedDocument.getId()));

        List<Document> documents = List.of(storedDocument);
        List<Directory> directories = List.of(storedDirectory);

        when(filesystemRedisService.getLatestSynchronizationTimestamp(clientID)).thenReturn(lastSyncTimestamp);

        when(directoryRepository.findAllByUserId(userID)).thenReturn(directories);
        when(documentRepository.findAllByUserId(userID)).thenReturn(documents);

        doNothing().when(filesystemRedisService).updateSynchronizationTimestamp(clientID, newSyncTimestamp);

        assertEquals(expectedResult, filesystemService.synchronize(clientID, userID));
    }

    @Test
    public void testSynchronizeWithExistingClientIdNoNewItems() {
        String clientID = "someid";
        int userID = 5;
        LocalDateTime newSyncTimestamp = mock(LocalDateTime.class);
        LocalDateTime latestTimeStamp = mock(LocalDateTime.class);
        Optional<LocalDateTime> lastSyncTimestamp = Optional.of(latestTimeStamp);


        SynchronizeResultDto expectedResult = new SynchronizeResultDto();
        expectedResult.setDirectoryIds(Collections.emptyList());
        expectedResult.setDocumentIds(Collections.emptyList());

        List<UUID> documentIds = Collections.emptyList();
        List<UUID> directoryIds = Collections.emptyList();

        when(filesystemRedisService.getLatestSynchronizationTimestamp(clientID)).thenReturn(lastSyncTimestamp);

        when(directoryRepository.findAllLatestBasedOnTimestamp(latestTimeStamp, userID)).thenReturn(directoryIds);
        when(documentRepository.findAllLatestBasedOnTimestamp(latestTimeStamp, userID)).thenReturn(documentIds);

        doNothing().when(filesystemRedisService).updateSynchronizationTimestamp(clientID, newSyncTimestamp);

        assertEquals(expectedResult, filesystemService.synchronize(clientID, userID));
    }
}
