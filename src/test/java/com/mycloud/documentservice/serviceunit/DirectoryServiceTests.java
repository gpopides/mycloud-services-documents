package com.mycloud.documentservice.serviceunit;

import com.mycloud.documentservice.beans.DirectoryManager;
import com.mycloud.documentservice.dto.ExportDirectoryResult;
import com.mycloud.documentservice.dto.FilesystemTree;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.services.DirectoryService;
import com.mycloud.documentservice.services.DirectoryServiceImpl;
import com.mycloud.documentservice.services.FilesystemResourceExtraActionService;
import com.mycloud.documentservice.services.RabbitMQPublisherService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import javax.persistence.EntityNotFoundException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

public class DirectoryServiceTests {
    private final DirectoryRepository directoryRepository = mock(DirectoryRepository.class);
    private final DirectoryManager directoryManager = mock(DirectoryManager.class);
    private final FilesystemResourceExtraActionService filesystemResourceExtraActionService = mock(FilesystemResourceExtraActionService.class);

    @InjectMocks
    private final DirectoryService directoryService = new DirectoryServiceImpl(
            directoryRepository,
            directoryManager,
            filesystemResourceExtraActionService
    );

    @Test
    public void testGetDirectoriesOfUser() {
        int userId = 5;
        List<Directory> directories = Collections.emptyList();

        when(directoryRepository.findAllByUserId(userId)).thenReturn(Collections.emptyList());

        assertEquals(directories, directoryService.getDirectories(userId));
    }

    @Test
    public void testDeleteDirectory() {
        UUID uuid = UUID.randomUUID();
        Optional<Directory> directoryFromDb = Optional.of(mock(Directory.class));

        String directoryPathStr = "somePath";
        Path directoryPath = mock(Path.class);

        //noinspection unchecked
        List<Directory> childrenDirectories = (List<Directory>) mock(List.class);

        when(directoryRepository.findById(uuid)).thenReturn(directoryFromDb);
        when(directoryRepository.getChildrenDirectories(uuid)).thenReturn(childrenDirectories);
        when(directoryFromDb.get().getPath()).thenReturn(directoryPathStr);

        doNothing().when(directoryRepository).delete(directoryFromDb.get());
        directoryRepository.delete(directoryFromDb.get());

        doNothing().when(directoryManager).deleteNonEmptyDirectory(directoryPath);
        directoryManager.deleteNonEmptyDirectory(directoryPath);

        assertTrue(directoryService.delete(uuid));
    }

    @Test
    public void testDeleteDirectoryInvalidId() {
        UUID uuid = UUID.randomUUID();

        when(directoryRepository.findById(uuid)).thenReturn(Optional.empty());

        assertThrows(
                EntityNotFoundException.class,
                () -> directoryService.delete(uuid)
        );
    }

    @Test
    public void getFilesystemStructure() {
        int userId = 5;
        Directory rootDirectory = mock(Directory.class);
        FilesystemTree fsTree = new FilesystemTree();

        when(directoryRepository.getRootDirectory(userId)).thenReturn(Optional.of(rootDirectory));

        fsTree.setFs(rootDirectory);

        assertEquals(fsTree, directoryService.getFilesystemStructure(userId));
    }

    @Test
    public void exportRootSucceeds() {
        Optional<Directory> rootDirectory = Optional.of(mock(Directory.class));
        int userId = 5;
        ExportDirectoryResult result = new ExportDirectoryResult();

        when(directoryRepository.getRootDirectory(userId)).thenReturn(rootDirectory);

        doAnswer(ans -> {
            result.setStarted(true);
            return null;
        }).when(directoryManager).zipDirectory(rootDirectory.get(), userId);


        assertEquals(result, directoryService.export(userId));
    }

    @Test
    public void exportRootFails() {
        Optional<Directory> rootDirectory = Optional.empty();
        int userId = 5;
        ExportDirectoryResult result = new ExportDirectoryResult();

        when(directoryRepository.getRootDirectory(userId)).thenReturn(rootDirectory);
        result.setStarted(false);
        result.setError("root directory for user 5 does not exist");

        assertEquals(result, directoryService.export(userId));
    }
}
