package com.mycloud.documentservice.serviceunit;

import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.serviceresult.UserDocumentsStats;
import com.mycloud.documentservice.services.StatisticsService;
import com.mycloud.documentservice.services.StatisticsServiceImpl;
import com.mycloud.documentservice.utils.GeneralUtils;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class StatisticsServiceTest {
    private final DirectoryRepository directoryRepository = mock(DirectoryRepository.class);
    private final DocumentRepository documentRepository = mock(DocumentRepository.class);

    @InjectMocks
    private final StatisticsService statisticsService = new StatisticsServiceImpl(
            documentRepository,
            directoryRepository
    );

    @Test
    public void getUserDocumentsStatsNoSubdirectories() {
        UserDocumentsStats expectedResult = new UserDocumentsStats();
        int userId = 5;
        int numOfDocumentsInt = 1;
        UUID directoryId = UUID.randomUUID();

        Directory dir = new Directory();
        dir.setDocuments(Set.of(mock(Document.class)));
        dir.setSubDirectories(Collections.emptySet());
        Optional<Directory> directory = Optional.of(dir);

        expectedResult.setNumberOfDocuments(numOfDocumentsInt);
        expectedResult.setTotalSize("0");

        when(directoryRepository.findById(directoryId)).thenReturn(directory);

        UserDocumentsStats actualStats = statisticsService.getUserDocumentsStats(userId, directoryId);

        assertEquals(expectedResult, actualStats);
        assertEquals(expectedResult.getNumberOfDocuments(), numOfDocumentsInt);
    }

    @Test
    public void getUserDocumentsStatsWithFileSize() {
        UserDocumentsStats expectedResult = new UserDocumentsStats();
        int userId = 5;
        int numOfDocumentsInt = 1;
        UUID directoryId = UUID.randomUUID();

        Document document = new Document();
        document.setSize(1024); // 1KB

        Directory dir = new Directory();
        dir.setDocuments(Set.of(document));
        dir.setSubDirectories(Collections.emptySet());
        Optional<Directory> directory = Optional.of(dir);

        expectedResult.setNumberOfDocuments(numOfDocumentsInt);
        expectedResult.setTotalSize(GeneralUtils.bytesToHumanFormat(1024));

        when(directoryRepository.findById(directoryId)).thenReturn(directory);

        UserDocumentsStats actualStats = statisticsService.getUserDocumentsStats(userId, directoryId);

        assertEquals(expectedResult, actualStats);
        assertEquals(expectedResult.getNumberOfDocuments(), numOfDocumentsInt);
    }

    @Test
    public void getUserDocumentsStatsNonExistingDirectory() {
        int userId = 5;
        UUID directoryId = UUID.randomUUID();

        Optional<Directory> directory = Optional.empty();


        when(directoryRepository.findById(directoryId)).thenReturn(directory);

        assertThrows(EntityNotFoundException.class , () -> statisticsService.getUserDocumentsStats(userId, directoryId));
    }
}
