package com.mycloud.documentservice.serviceunit;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.MoveEntityDto;
import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentInformationDto;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.enums.EntityType;
import com.mycloud.documentservice.exceptions.InvalidUploadException;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.serviceresult.UploadDocumentServiceResult;
import com.mycloud.documentservice.services.*;
import com.mycloud.documentservice.utils.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DocumentServiceTests {
   private final DirectoryRepository directoryRepository = mock(DirectoryRepository.class);
   private final DocumentRepository documentRepository = mock(DocumentRepository.class);
   private final DocumentManager documentManager = mock(DocumentManager.class);
   private final FilesystemResourceExtraActionService filesystemResourceExtraActionService = mock(FilesystemResourceExtraActionService.class);

   @InjectMocks
   private final DocumentService documentService = new DocumentServiceImpl(
           documentRepository,
           directoryRepository,
           documentManager,
           filesystemResourceExtraActionService
   );

   @Test
   public void testUploadFailsBecauseUploadIdIsInvalid() {
      UploadDocumentDto documentDto = mock(UploadDocumentDto.class);

      when(documentManager.uploadIsValid(documentDto)).thenReturn(false);
      Assertions.assertThrows(InvalidUploadException.class, () -> documentService.uploadDocument(documentDto));
   }

   @Test
   public void testUploadFailsBecauseInvalidDirectoryId() {
      UploadDocumentDto documentDto = mock(UploadDocumentDto.class);

      UUID directoryId = UUID.randomUUID();

      when(documentManager.uploadIsValid(documentDto)).thenReturn(true);
      when(directoryRepository.existsById(directoryId)).thenReturn(false);
      Assertions.assertThrows(InvalidUploadException.class, () -> documentService.uploadDocument(documentDto));
   }

   @Test
   public void testUploadSavesChunkSuccessfully() {
      UploadDocumentDto documentDto = mock(UploadDocumentDto.class);
      UploadDocumentServiceResult expectedResult = new UploadDocumentServiceResult(false);

      UUID dirId = UUID.randomUUID();
      when(documentDto.getDirectoryId()).thenReturn(dirId);
      when(documentManager.uploadIsValid(documentDto)).thenReturn(true);
      when(directoryRepository.existsById(documentDto.getDirectoryId())).thenReturn(true);

      float percentage = 26.65f;
      expectedResult.setSaved(true);
      expectedResult.setPercentage(percentage);

      when(documentManager.uploadIsValid(documentDto)).thenReturn(true);
      when(documentManager.isFullUpload(documentDto.getUploadId())).thenReturn(false);
      when(documentManager.saveChunk(documentDto)).thenReturn(true);
      when(documentManager.calculateUploadPercentage(documentDto)).thenReturn(percentage);

      when(documentManager.isFinalChunk(documentDto.chunkIdSequence(), documentDto.getUploadId())).thenReturn(false);

      Assertions.assertEquals(expectedResult, documentService.uploadDocument(documentDto));
      Assertions.assertFalse(expectedResult.isCompleted());
   }

   @Test
   public void testUploadInvalidDirectoryId() {
      UploadDocumentDto documentDto = mock(UploadDocumentDto.class);
      UUID directoryId = UUID.randomUUID();

      when(documentDto.getDirectoryId()).thenReturn(directoryId);
      when(documentManager.uploadIsValid(documentDto)).thenReturn(true);
      when(directoryRepository.existsById(documentDto.getDirectoryId())).thenReturn(true);

      when(documentManager.isFinalChunk(documentDto.chunkIdSequence(), documentDto.getUploadId())).thenReturn(true);

      Optional<Directory> emptyDirectory = Optional.empty();

      when(documentDto.getDirectoryId()).thenReturn(directoryId);
      when(documentManager.isFullUpload(documentDto.getUploadId())).thenReturn(false);
      when(directoryRepository.findById(directoryId)).thenReturn(emptyDirectory);

      Assertions.assertThrows(EntityNotFoundException.class , () -> documentService.uploadDocument(documentDto));
   }

   @Test
   public void testUploadFinalChunkSuccessfully() {
      UploadDocumentDto documentDto = mock(UploadDocumentDto.class);
      UUID directoryId = UUID.randomUUID();
      UploadDocumentServiceResult expectedResult = new UploadDocumentServiceResult(false);

      int chunkSequence = 5;
      String uploadId = "asd";

      when(documentDto.getDirectoryId()).thenReturn(directoryId);
      when(documentManager.uploadIsValid(documentDto)).thenReturn(true);
      when(directoryRepository.existsById(documentDto.getDirectoryId())).thenReturn(true);

      when(documentDto.chunkIdSequence()).thenReturn(chunkSequence);
      when(documentDto.getUploadId()).thenReturn(uploadId);
      when(documentDto.getDirectoryId()).thenReturn(directoryId);

      when(documentManager.isFullUpload(documentDto.getUploadId())).thenReturn(false);

      when(documentManager.isFinalChunk(documentDto.chunkIdSequence(), documentDto.getUploadId())).thenReturn(true);


      Optional<Directory> directory = Optional.of(mock(Directory.class));

      when(directoryRepository.findById(directoryId)).thenReturn(directory);

      String directoryPath = "somepath";
      when(directory.get().getPath()).thenReturn(directoryPath);

      //noinspection unchecked
      doAnswer(answer -> {
         expectedResult.setPercentage(100.00f);
         expectedResult.setSaved(true);
         expectedResult.setCompleted(true);
         return null;
      }).when(documentManager).mergeChunks(any(String.class), any(String.class), any(Consumer.class));

      doNothing().when(filesystemResourceExtraActionService).publishToThirdPartyServices(mock(Document.class), Document.class);
      doNothing().when(documentManager).deleteUploadId(uploadId);

      documentService.uploadDocument(documentDto);

      assertTrue(expectedResult.isCompleted());
      assertTrue(expectedResult.isSaved());
      assertEquals(expectedResult.getPercentage(), 100.00f);
   }

   @Test
   public void retrievingUploadInformationFailsInvalidDirectoryId() {
      UploadDocumentInformationDto uploadDocumentInformationDto = mock(UploadDocumentInformationDto.class);
      UUID directoryId = UUID.randomUUID();

      when(uploadDocumentInformationDto.getDirectoryId()).thenReturn(directoryId);
      when(directoryRepository.existsById(uploadDocumentInformationDto.getDirectoryId())).thenReturn(false);

      Assertions.assertThrows(
              InvalidUploadException.class ,
              () -> documentService.getUploadDocumentStatistics(uploadDocumentInformationDto),
              "invalidDirectory"
      );
   }

   @Test
   public void getDocumentContentSuccess() {
      UUID documentId = UUID.randomUUID();

      Document document = mock(Document.class);
      Optional<Document> documentFromDb = Optional.of(document);

      when(documentRepository.findById(documentId)).thenReturn(documentFromDb);

      String documentPath = "somepath";
      when(document.getPath()).thenReturn(documentPath);

      byte[] content = "File content".getBytes();

      when(documentManager.fileContent(document.getPath())).thenReturn(content);
      assertNotNull(documentManager.fileContent(document.getPath()));
      assertEquals(content, documentManager.fileContent(document.getPath()));
      assertEquals(content.length, documentManager.fileContent(document.getPath()).length);
   }

   @Test
   public void getDocumentFailsInvalidDocumentId() {
      UUID documentId = UUID.randomUUID();

      when(documentRepository.findById(documentId)).thenReturn(Optional.empty());

      Assertions.assertThrows(
              EntityNotFoundException.class ,
              () -> documentService.getDocument(documentId, "name")
      );
   }

   @Test
   public void getDocumentFailsInvalidName() {
      UUID documentId = UUID.randomUUID();
      Document document = mock(Document.class);

      Optional<Document> documentFromDb = Optional.of(document);
      String documentName = "str";

      when(documentRepository.findById(documentId)).thenReturn(documentFromDb);

      when(document.getName()).thenReturn(documentName);

      Assertions.assertThrows(
              EntityNotFoundException.class ,
              () -> documentService.getDocument(documentId, "name")
      );
   }

   @Test
   public void getDocumentsByType() {
      UUID userID = UUID.randomUUID();
      DocumentType type = DocumentType.IMAGE;
      Iterable<Document> documentsFromDb = List.of(mock(Document.class));


      when(documentRepository.findDocumentsOfUserByType(type, userID)).thenReturn(documentsFromDb);

      for (Document d: documentsFromDb) {
         when(d.getId()).thenReturn(userID);
         when(d.getName()).thenReturn("name.txt");
         when(d.getDocumentType()).thenReturn(type);
      }

      Iterable<DocumentDto> documentDtos = documentService.getDocumentsByType(type, userID);

      long actualItemsCount = CollectionUtils.iterableSize(documentsFromDb);

      assertEquals(actualItemsCount, CollectionUtils.iterableSize(documentDtos));
   }

   @Test
   public void moveDocuments() {
      UUID destinationDirectoryId = UUID.randomUUID();
      MoveEntityDto moveItem = new MoveEntityDto();
      moveItem.setId(UUID.randomUUID());
      moveItem.setEntityType(EntityType.DOCUMENT);

      List<MoveEntityDto> itemsToMove = List.of(moveItem);
      Directory destinationDirectory = new Directory();
      destinationDirectory.setPath("somepath");

      when(directoryRepository.findById(destinationDirectoryId)).thenReturn(Optional.of(destinationDirectory));

      String updatedDocumentName = "somename";
      Document updatedDocument = new Document();
      updatedDocument.setName(updatedDocumentName);
      updatedDocument.setPath(destinationDirectory.getPath() + "/" + updatedDocument.getName());

      List<Document> updatedDocuments =  List.of(updatedDocument);
      List<Directory> updatedDirectories =  List.of(mock(Directory.class));

      when(documentRepository.saveAll(updatedDocuments)).thenReturn(updatedDocuments);
      when(directoryRepository.saveAll(updatedDirectories)).thenReturn(updatedDirectories);

      documentService.moveItemsToDirectory(destinationDirectoryId, itemsToMove);

      for (Document d: updatedDocuments) {
          String expectedPath = destinationDirectory.getPath() + "/" + d.getName();
          assertEquals(expectedPath, d.getPath());
      }
   }

   @Test
   public void moveDirectories() {
      UUID destinationDirectoryId = UUID.randomUUID();
      MoveEntityDto moveItem = new MoveEntityDto();
      moveItem.setId(UUID.randomUUID());
      moveItem.setEntityType(EntityType.DIRECTORY);

      List<MoveEntityDto> itemsToMove = List.of(moveItem);
      Directory destinationDirectory = new Directory();
      destinationDirectory.setPath("somepath");

      when(directoryRepository.findById(destinationDirectoryId)).thenReturn(Optional.of(destinationDirectory));

      String updatedDocumentName = "somename";
      Directory updatedDirectory = new Directory();
      updatedDirectory.setName(updatedDocumentName);
      updatedDirectory.setPath(destinationDirectory.getPath() + "/" + updatedDirectory.getName());

      List<Document> updatedDocuments =  List.of(mock(Document.class));
      List<Directory> updatedDirectories =  List.of(updatedDirectory);

      when(documentRepository.saveAll(updatedDocuments)).thenReturn(updatedDocuments);
      when(directoryRepository.saveAll(updatedDirectories)).thenReturn(updatedDirectories);

      documentService.moveItemsToDirectory(destinationDirectoryId, itemsToMove);

      for (Directory d: updatedDirectories) {
         String expectedPath = destinationDirectory.getPath() + "/" + d.getName();
         assertEquals(expectedPath, d.getPath());
      }
   }
}
