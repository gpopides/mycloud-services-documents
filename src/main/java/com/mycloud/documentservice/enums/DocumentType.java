package com.mycloud.documentservice.enums;

public enum DocumentType {
	IMAGE,
	TEXT_FILE,
	VIDEO,
	ZIP
}
