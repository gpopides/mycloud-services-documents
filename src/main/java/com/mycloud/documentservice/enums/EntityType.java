package com.mycloud.documentservice.enums;

import com.mycloud.documentservice.dto.MoveEntityDto;

public enum EntityType {
	DOCUMENT,
	DIRECTORY;

	public static boolean isDocument(MoveEntityDto item) {
		return item.getEntityType() == DOCUMENT;
	}

	public static boolean isDirectory(MoveEntityDto item) {
		return item.getEntityType() == DIRECTORY;
	}
}
