package com.mycloud.documentservice.responses;

import lombok.Data;

@Data
public class RegisterServicesResponse {
	private boolean registered;
}
