package com.mycloud.documentservice.responses;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CollectionResponse<T> {
	public Iterable<T> result;
}
