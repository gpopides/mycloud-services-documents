package com.mycloud.documentservice.responses;

import com.mycloud.documentservice.dto.DocumentDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class LatestUploadStatistics {
	private final Iterable<DocumentDto> documents;
}
