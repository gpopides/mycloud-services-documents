package com.mycloud.documentservice.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeleteEntityResponse {
	private boolean deleted;
}
