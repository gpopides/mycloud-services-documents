package com.mycloud.documentservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mycloud.documentservice.enums.DocumentType;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.UUID;

@Entity
@Table(name = "documents")
@Data
@NoArgsConstructor
public class Document {
	@Id
	@Type(type = "pg-uuid")
	private UUID id;

	@Column(name = "document_type")
	@Enumerated(EnumType.STRING)
	private DocumentType documentType;

	@Column(name = "name", length = 100)
	private String name;

	@Column(name = "uploaded_at")
	private LocalDateTime uploadedAt;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "directory_id", nullable = false)
	@JsonIgnore
	private Directory directory;

	@Column(name = "path", length = 200)
	private String path;

	@Column(name = "size")
	private long size;

	private Document(Builder builder) {
		this.id = builder.getId();
		this.directory = builder.getDirectory();
		this.documentType = builder.getType();
		this.uploadedAt = builder.getUploadedAt();
		this.name = builder.getName();
		this.path = builder.getPath().toString();
		this.size = builder.getSize();
	}

	public void updateFields(Document newDocument) {
		this.directory = newDocument.getDirectory();
		this.documentType = newDocument.getDocumentType();
		this.uploadedAt = newDocument.getUploadedAt();
		this.name = newDocument.getName();
		this.path = newDocument.getPath();
	}

	public static Comparator<Document> dateComparator() {
		return Comparator.comparing(Document::getUploadedAt);
	}


	@Getter
	public static class Builder {
		private final UUID id;
		private String name;
		private LocalDateTime uploadedAt;
		private Directory directory;
		private Path path;
		private DocumentType type;
		private long size;

		public Builder() {
			this.id = UUID.randomUUID();
		}

		public Builder setDirectory(Directory directory) {
			this.directory = directory;
			return this;
		}

		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setUploadedAt(LocalDateTime uploadedAt) {
			this.uploadedAt = uploadedAt;
			return this;
		}

		public Builder setPath(Path path) {
			this.path = path;
			return this;
		}

		public Builder setType(DocumentType type) {
			this.type = type;
			return this;
		}

		public Builder setSize(long size) {
			this.size = size;
			return this;
		}

		public Document build() {
			return new Document(this);
		}
	}
}
