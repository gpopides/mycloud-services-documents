package com.mycloud.documentservice.entities;

import lombok.Data;

import java.util.UUID;

@Data
public class FilesystemResource {
	UUID id;
}
