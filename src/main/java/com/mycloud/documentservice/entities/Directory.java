package com.mycloud.documentservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "directories")
@Getter
@Setter
@NoArgsConstructor
public class Directory {

	@Id
	@Type(type = "pg-uuid")
	private UUID id;

	@Column(name = "name")
	private String name;

	@Column(name = "path")
	private String path;

	@Column(name = "user_id")
	private int userId;

	@OneToMany(mappedBy = "directory", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@OrderBy("uploadedAt")
	private Set<Document> documents;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@JsonIgnore
	private Directory parentDir;

	@OneToMany(mappedBy = "parentDir", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Directory> subDirectories;

	@Column(name = "uploaded_at")
	private LocalDateTime uploadedAt;

	public Directory(String path, String name, int userId) {
		this.id = UUID.randomUUID();
		this.name = name;
		this.path = path;
		this.userId = userId;
		this.uploadedAt = LocalDateTime.now();
	}

	public String normalizePath() {
		final Path basePath = Paths.get(System.getProperty("user.home"), ".mycloud");
		return path.replace(basePath.toString(), "");
	}

}
