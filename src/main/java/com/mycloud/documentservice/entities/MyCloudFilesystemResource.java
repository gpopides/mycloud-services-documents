package com.mycloud.documentservice.entities;

public interface MyCloudFilesystemResource<T> {
	void moveTo(Directory destination);
}
