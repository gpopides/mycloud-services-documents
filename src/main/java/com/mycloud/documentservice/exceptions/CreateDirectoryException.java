package com.mycloud.documentservice.exceptions;

import com.mycloud.documentservice.serviceresult.CreateDirectoryResult;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class CreateDirectoryException extends RuntimeException {
	public CreateDirectoryException(CreateDirectoryResult result) {
		super("yo");
		this.result = result;
	}

	private CreateDirectoryResult result;

	public Map<String, String> getResultAsMap() {
		return Map.of(
				"error", result.getError()
		);
	}

	public HttpStatus getStatus() {
		return result.getStatus();
	}
}
