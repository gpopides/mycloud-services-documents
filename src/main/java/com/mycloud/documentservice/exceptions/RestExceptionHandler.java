package com.mycloud.documentservice.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.Map;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(value = {CreateDirectoryException.class})
	protected ResponseEntity<Object> handleConflict(RuntimeException exception, WebRequest request) {
		Map<String, String> body = ((CreateDirectoryException) exception).getResultAsMap();
		HttpStatus status = ((CreateDirectoryException) exception).getStatus();

		return handleExceptionInternal(exception, body, new HttpHeaders(), status, request);
	}

	@ExceptionHandler(value = {EntityNotFoundException.class})
	protected ResponseEntity<Object> handleNotFound(RuntimeException exception, WebRequest request) {

		Map<String, String> body = Map.of("error", exception.getMessage());
		HttpStatus status = HttpStatus.NOT_FOUND;

		return handleExceptionInternal(exception, body, new HttpHeaders(), status, request);
	}

	@ExceptionHandler(value = {InvalidUploadException.class, UnsupportedThumbnailDocumentException.class})
	protected ResponseEntity<Object> handleBadRequest(RuntimeException exception, WebRequest request) {

		Map<String, String> body = Map.of("error", exception.getMessage());
		HttpStatus status = HttpStatus.BAD_REQUEST;

		return handleExceptionInternal(exception, body, new HttpHeaders(), status, request);
	}

}
