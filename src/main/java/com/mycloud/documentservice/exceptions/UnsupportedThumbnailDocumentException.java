package com.mycloud.documentservice.exceptions;

public class UnsupportedThumbnailDocumentException extends RuntimeException {
	public UnsupportedThumbnailDocumentException(String message) {
		super(message);
	}
}
