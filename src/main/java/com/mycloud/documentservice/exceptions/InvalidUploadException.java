package com.mycloud.documentservice.exceptions;

public class InvalidUploadException extends RuntimeException {
	public InvalidUploadException(String message) {
		super(message);
	}
}
