package com.mycloud.documentservice.exceptions;

public class UserExportException extends RuntimeException {
	public UserExportException(String msg) {
		super(msg);
	}
}
