package com.mycloud.documentservice.rabbitmqmessage;

import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.EntityType;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

@Data
@Getter
public class NewEntityCreatedMessage implements Serializable {
	private UUID id;
	private EntityType type;
	private int userId;

	private NewEntityCreatedMessage(UUID id, int userId, EntityType type) {
		this.id = id;
		this.type = type;
		this.userId = userId;
	}

	public static NewEntityCreatedMessage of(Document document) {
		return new NewEntityCreatedMessage(
				document.getId(),
				document.getDirectory().getUserId(),
				EntityType.DOCUMENT
		);
	}

	public static NewEntityCreatedMessage of(Directory directory) {
		return new NewEntityCreatedMessage(
				directory.getId(),
				directory.getUserId(),
				EntityType.DIRECTORY
		);
	}
}
