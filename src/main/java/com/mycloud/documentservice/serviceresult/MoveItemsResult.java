package com.mycloud.documentservice.serviceresult;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoveItemsResult {
	private boolean moved = false;
	private String error;
}
