package com.mycloud.documentservice.serviceresult;

import lombok.Data;

@Data
public class UserDocumentsStats {
	private String totalSize;
	private int numberOfDocuments;
	private int numberOfSubDirectories;
}
