package com.mycloud.documentservice.serviceresult;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseServiceResult {
	private String error;
	private HttpStatus status;

}
