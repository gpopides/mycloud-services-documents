package com.mycloud.documentservice.serviceresult;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mycloud.documentservice.dto.DocumentDto;
import lombok.Data;


@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadDocumentServiceResult {
	private boolean saved;
	private float percentage;
	private DocumentDto document;
	private boolean completed;
	private boolean alreadyExisted;

	public UploadDocumentServiceResult(boolean completed) {
		this.completed = completed;
		percentage = 0f;
	}

	public void setFinished() {
		this.percentage = 100.00f;
		this.saved = true;
		this.completed = true;
	}
}
