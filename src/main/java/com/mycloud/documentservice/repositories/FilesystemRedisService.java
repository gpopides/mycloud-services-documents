package com.mycloud.documentservice.repositories;

import java.time.LocalDateTime;
import java.util.Optional;

public interface FilesystemRedisService {
	Optional<LocalDateTime> getLatestSynchronizationTimestamp(String clientId);

	void updateSynchronizationTimestamp(String clientId, LocalDateTime dateTime);

	void deleteSynchronizationTimestamp(String clientId);
}
