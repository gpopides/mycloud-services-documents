package com.mycloud.documentservice.repositories;

import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.services.DocumentRedisRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DocumentRepository extends
		CrudRepository<Document, UUID>, JpaRepository<Document, UUID>,
		DocumentRedisRepository {
	Optional<Document> findByPath(String path);

	@Query("select d from Document d inner JOIN Directory dir on dir.id = d.directory.id where dir.userId = :userId")
	Iterable<Document> findAllByUserId(int userId);

	@Query(value = "" +
			"select d from Document d inner JOIN Directory dir on dir.id = d.directory.id " +
			"where dir.userId = :userId and d.documentType = :documentType " +
			"order by d.uploadedAt desc"
	)
	Iterable<Document> findDocumentsOfUserByType(DocumentType documentType, UUID userId);

	//    @Query("select d.id from Document d where d.uploadedAt > :timestamp")
	@Query(value = "" +
			"select d.id from Document d inner JOIN Directory dir on dir.id = d.directory.id " +
			"where dir.userId = :userId and d.uploadedAt > :timestamp"
	)
	Iterable<UUID> findAllLatestBasedOnTimestamp(LocalDateTime timestamp, int userId);

	@Query(value = "" +
			"select d from Document d left JOIN Directory dir on dir.id = d.directory.id " +
			"where dir.userId = :userId order by d.uploadedAt")
	List<Document> getLatestUploads(int userId, Pageable pageable);

	default List<Document> fetchLatestDocuments(int userId, int limit) {
		return getLatestUploads(userId, PageRequest.of(0, limit, Sort.Direction.ASC, "uploadedAt"));
	}
}
