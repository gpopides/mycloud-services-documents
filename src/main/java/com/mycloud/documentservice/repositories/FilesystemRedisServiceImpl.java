package com.mycloud.documentservice.repositories;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class FilesystemRedisServiceImpl implements FilesystemRedisService {
	private final ValueOperations<String, String> redis;
	private final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

	public FilesystemRedisServiceImpl(
			@Qualifier("synchronizeFilesystemRedisTemplate") RedisTemplate<String, String> redisTemplate
	) {
		redis = redisTemplate.opsForValue();
	}

	@Override
	public Optional<LocalDateTime> getLatestSynchronizationTimestamp(String clientId) {
		Optional<String> optionalStoredTimestamp = Optional.ofNullable(redis.get(constructRedisKey(clientId)));

		return optionalStoredTimestamp.map(s -> LocalDateTime.parse(s, formatter));
	}

	@Override
	public void updateSynchronizationTimestamp(String clientId, LocalDateTime dateTime) {
		redis.set(constructRedisKey(clientId), formatter.format(dateTime));
	}

	@Override
	public void deleteSynchronizationTimestamp(String clientId) {
		// value ops does not have delete methdo
		// so set a tmp value to the given key with a small expiry duration
		redis.set(constructRedisKey(clientId), formatter.format(LocalDateTime.now()), Duration.ofMillis(10));
	}

	private String constructRedisKey(String clientId) {
		return "MYCLOUD_SYNCHRONIZE" + "_" + clientId;
	}
}
