package com.mycloud.documentservice.repositories;

import com.mycloud.documentservice.entities.Directory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.UUID;

@Component
@AllArgsConstructor
public class DirectoryRepositoryImpl implements DirectoryRepositoryCustom {
	private final DirectoryRepository directoryRepository;

	@Override
	public Directory findOrFail(UUID id) {
		Optional<Directory> directory = directoryRepository.findById(id);

		if (directory.isEmpty()) {
			throw new EntityNotFoundException();
		}

		return directory.get();
	}
}
