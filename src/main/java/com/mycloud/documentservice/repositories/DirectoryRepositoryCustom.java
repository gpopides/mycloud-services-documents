package com.mycloud.documentservice.repositories;

import com.mycloud.documentservice.entities.Directory;

import java.util.UUID;

public interface DirectoryRepositoryCustom {
	Directory findOrFail(UUID id);
}
