package com.mycloud.documentservice.repositories;

import com.mycloud.documentservice.entities.Directory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DirectoryRepository extends CrudRepository<Directory, UUID>, JpaRepository<Directory, UUID> {
	Iterable<Directory> findAllByUserId(int userId);

	@Query("select d from Directory d where d.parentDir.id = :directoryId")
	Iterable<Directory> getChildrenDirectories(@Param("directoryId") UUID directoryId);

	Iterable<Directory> findByPathIn(List<String> paths);

	@Query("select d from Directory d where d.userId =:userId and d.name = 'root' and d.parentDir.id is null")
	Optional<Directory> getRootDirectory(int userId);

	@Query("select count(d) from Directory d where d.parentDir.id = :directoryId")
	long subdirectoriesOfDirectory(UUID directoryId);

	@Query("select d.id from Directory d where d.uploadedAt > :timestamp and d.userId = :userId")
	Iterable<UUID> findAllLatestBasedOnTimestamp(LocalDateTime timestamp, int userId);
}
