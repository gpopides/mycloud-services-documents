package com.mycloud.documentservice.listeners;

import com.mycloud.documentservice.requests.RegisterServicesRequest;
import com.mycloud.documentservice.responses.RegisterServicesResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
@AllArgsConstructor
public class EndpointsListener implements ApplicationListener<ContextRefreshedEvent> {
	private final Environment environment;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (!List.of(environment.getActiveProfiles()).contains("test")) {
			List<Method> controllerMethods = event.getApplicationContext()
					.getBean(RequestMappingHandlerMapping.class)
					.getHandlerMethods()
					.values()
					.stream()
					.map(HandlerMethod::getMethod)
					.filter(method -> isControllerOfTheApplication(method.getDeclaringClass().getName()))
					.collect(Collectors.toList());

			registerServices(getEndpoints(controllerMethods));
		}
	}

	private void registerServices(List<String> endpoints) {
		RegisterServicesRequest request = new RegisterServicesRequest(
				endpoints,
				"documents-service",
				"http://localhost:8080"
		);

		WebClient.builder()
				.baseUrl("http://localhost:4001")
				.build()
				.post()
				.uri("/register")
				.body(BodyInserters.fromValue(request))
				.retrieve()
				.bodyToMono(RegisterServicesResponse.class)
				.doOnError(error -> log.error("Could not register service to the service registry"))
				.subscribe(response -> {
					if (response.isRegistered()) {
						log.info("Services registered to service registry");
					} else {
						log.error("Could not register service to the service registry");
					}
				});
	}

	private List<String> getEndpoints(List<Method> controllerMethods) {
		List<String> endpoints = new ArrayList<>();

		for (Method method : controllerMethods) {
			for (Annotation declaredAnnotation : method.getDeclaredAnnotations()) {
				Class<? extends Annotation> annotationType = declaredAnnotation.annotationType();

				if (isMappingAnnotation(annotationType)) {
					endpoints.add(getMappingPath(method, annotationType));
				}
			}
		}
		return endpoints;
	}

	private boolean isControllerOfTheApplication(String controllerName) {
		return controllerName.startsWith("com.mycloud.documentservice.controllers");
	}

	private boolean isMappingAnnotation(Class<? extends Annotation> annotationType) {
		List<Class<?>> mappingClasses = List.of(
				GetMapping.class,
				PostMapping.class,
				DeleteMapping.class,
				PutMapping.class
		);
		return mappingClasses.stream().anyMatch(mappingClass -> mappingClass.equals(annotationType));
	}

	private String getMappingPath(Method method, Class<? extends Annotation> mappingAnnotation) {
		final String PREFIX = "api/";
		String path;

		if (mappingAnnotation.equals(GetMapping.class)) {
			path = String.join("", method.getAnnotation(GetMapping.class).value());
		} else if (mappingAnnotation.equals(PostMapping.class)) {
			path = String.join("", method.getAnnotation(PostMapping.class).value());
		} else if (mappingAnnotation.equals(PutMapping.class)) {
			path = String.join("", method.getAnnotation(PutMapping.class).value());
		} else {
			path = String.join("", method.getAnnotation(DeleteMapping.class).value());
		}
		return PREFIX.concat(path);
	}

}
