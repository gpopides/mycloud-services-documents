package com.mycloud.documentservice.beans;

import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.serviceresult.CreateDirectoryResult;

import java.nio.file.Path;

public interface DirectoryManager {
	Path getBaseUserPath(int userId);

	Path constructTmpUploadPath(String uploadId, String... parts);

	void deleteNonEmptyDirectory(Path directory);

	boolean parentIsRoot(String path);

	String directoryName(String path);

	String directoryName(Path path);

	CreateDirectoryResult createRootDirectory(int userId);

	String getParentDirectory(Directory directory);

	void moveAndUpdateAttributes(Directory directory, Directory destination);

	void zipDirectory(Directory outputDirectory, int userId);
}
