package com.mycloud.documentservice.beans;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public interface FileManager {
	static void moveTo(String source, String destinationDirectory) {
		try {
			Path destination = getDestinationFullPath(source, destinationDirectory);
			Files.move(Path.of(source), destination);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static Path getDestinationFullPath(String source, String destinationDirectory) {
		// get the name of the source item, and append it to the destination directory,
		// so it works like the mv command.
		Path _source = Path.of(source);
		String sourceItemName = _source.getFileName().toString();
		return Path.of(destinationDirectory, sourceItemName);
	}
}
