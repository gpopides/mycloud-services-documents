package com.mycloud.documentservice.beans;

import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.exceptions.InvalidUploadException;
import com.mycloud.documentservice.exceptions.UserExportException;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.serviceresult.CreateDirectoryResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
@Slf4j
@AllArgsConstructor
public class DirectoryManagerImpl implements DirectoryManager {
	private final DocumentRepository documentRepository;
	private final Path basePath = Paths.get(System.getProperty("user.home"), ".mycloud");

	@PostConstruct
	public void createStartupDirectories() {
		if (!Files.exists(basePath)) {
			try {
				Files.createDirectory(basePath);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}

	public Path getBaseUserPath(int userId) {
		return Paths.get(basePath.toString(), String.valueOf(userId));
	}

	@Override
	public Path constructTmpUploadPath(String uploadId, String... parts) {
		Path uploadDirectory = Paths.get(basePath.toString(), "tmp", uploadId);
		try {
			Files.createDirectories(uploadDirectory);
		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidUploadException("failedToSaveChunk");
		}
		return Paths.get(uploadDirectory.toString(), parts);
	}

	public void deleteNonEmptyDirectory(Path directory) {
		try {
			Files.list(directory).forEach(f -> {
				try {
					Files.delete(f);
				} catch (IOException e) {
					// log
				}
			});
			Files.delete(directory);
		} catch (IOException e) {
			e.printStackTrace();
			// log
		}
	}

	public boolean parentIsRoot(String path) {
		return Path.of(path).getParent().toString().equals(basePath.toString());
	}

	@Override
	public String directoryName(String path) {
		return directoryName(Path.of(path));
	}

	@Override
	public String directoryName(Path path) {
		return path.getFileName().toString();
	}

	@Override
	public CreateDirectoryResult createRootDirectory(int userId) {
		CreateDirectoryResult result = new CreateDirectoryResult();
		try {
			Path path = Paths.get(basePath.toString(), String.valueOf(userId));
			Files.createDirectory(path);
			result.setCreated(true);
			result.setPath(path.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getParentDirectory(Directory directory) {
		return new File(directory.getPath()).getParent();
	}

	@Override
	public void moveAndUpdateAttributes(Directory directory, Directory destination) {

		FileManager.moveTo(directory.getPath(), destination.getPath());

		String newPath = FileManager.getDestinationFullPath(
				directory.getPath(),
				destination.getPath()
		).toString();

		directory.setParentDir(destination);
		directory.setPath(newPath);

		// after moving the directory and changing it's parent dir, we need to update the documents path
		// inside this directory and the subdirectories with their documents and so on.
		updatePathOfDocuments(directory.getDocuments(), newPath);
		updatePathOfSubdirectories(directory.getSubDirectories(), newPath);
	}

	private void updatePathOfSubdirectories(Set<Directory> subDirs, String newPath) {
		subDirs.forEach(subdir -> {
			String newDestinationPath = FileManager.getDestinationFullPath(subdir.getPath(), newPath).toString();
			subdir.setPath(newDestinationPath);
			updatePathOfDocuments(subdir.getDocuments(), newDestinationPath);
			updatePathOfSubdirectories(subdir.getSubDirectories(), newDestinationPath);
		});
	}

	private void updatePathOfDocuments(Set<Document> documents, String newPath) {
		documents.forEach(d -> {
			String t = FileManager.getDestinationFullPath(d.getPath(), newPath).toString();
			d.setPath(t);
		});
	}

	/**
	 * @param outputDirectory {@link com.mycloud.documentservice.entities.Directory} the directory entity
	 *                        which will be used to save the zip
	 *                        <p>
	 *                        <p>
	 *                        Takes the path and creates a zip. Normally, this method would be used  to zip all the files of a user
	 */
	@Async
	public void zipDirectory(Directory outputDirectory, int userId) {
		if (Files.notExists(Path.of(outputDirectory.getPath()))) {
			String msg = String.format("%s not found, could not export", outputDirectory.getPath());
			log.error(msg);
			throw new UserExportException("Could not export data");
		}

		Path exportZipPath = Paths.get(basePath.toString(), Integer.toString(userId), "export.zip");

		try (
				FileOutputStream fos = new FileOutputStream(exportZipPath.toString());
				ZipOutputStream zos = new ZipOutputStream(fos)
		) {
			File outputZip = new File(outputDirectory.getPath());
			addToZip(outputZip, outputZip.getName(), zos);

			Document exportDocument = new Document.Builder()
					.setName("export.zip")
					.setDirectory(outputDirectory)
					.setPath(exportZipPath)
					.setSize(Files.size(exportZipPath))
					.setType(DocumentType.ZIP)
					.setUploadedAt(LocalDateTime.now())
					.build();

			documentRepository.save(exportDocument);

		} catch (IOException e) {
			log.error(e.getMessage());
		}

	}

	private void addToZip(File itemToAddToZip, String entryName, ZipOutputStream zipOutputStream) {
		try {
			if (itemToAddToZip.isDirectory()) {
				String directoryEntryName = entryName + File.separator;
				// if its a directory, add a directory entry to the zip
				// and loop it's content and call recursively the zip method on each of it's children
				zipOutputStream.putNextEntry(new ZipEntry(directoryEntryName));
				zipOutputStream.closeEntry();

				Files.list(Path.of(itemToAddToZip.getPath())).forEach(file -> {
					String fileEntryName = String.format("%s%s%s", directoryEntryName, File.separator, file.getFileName());
					addToZip(new File(file.toString()), fileEntryName, zipOutputStream);
				});
			} else {
				ZipEntry ze = new ZipEntry(entryName + Paths.get(itemToAddToZip.getPath()).getFileName());
				zipOutputStream.putNextEntry(ze);
				zipOutputStream.write(Files.readAllBytes(Path.of(itemToAddToZip.getPath())));
				zipOutputStream.closeEntry();
			}
		} catch (IOException e) {
			log.error(String.format("Could not add %s to export zip", entryName));
			e.printStackTrace();
		}
	}

}
