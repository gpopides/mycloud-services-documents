package com.mycloud.documentservice.controllers;

import com.mycloud.documentservice.dto.SynchronizeResultDto;
import com.mycloud.documentservice.responses.ResultsResponse;
import com.mycloud.documentservice.services.FilesystemService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
public class FilesystemController {
	private final FilesystemService filesystemService;

	@GetMapping("filesystem/{userId}/synchronize")
	public ResultsResponse<SynchronizeResultDto> synchronize(
			@RequestParam(name = "cid") String clientId,
			@PathVariable int userId) {

		return new ResultsResponse<>(filesystemService.synchronize(clientId, userId));
	}
}
