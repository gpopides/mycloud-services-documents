package com.mycloud.documentservice.controllers;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.dto.LatestDocumentsFilter;
import com.mycloud.documentservice.dto.MoveEntitiesRequest;
import com.mycloud.documentservice.dto.ShareDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentInformationDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;
import com.mycloud.documentservice.elasticdto.UploadItem;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.responses.CollectionResponse;
import com.mycloud.documentservice.responses.DeleteEntityResponse;
import com.mycloud.documentservice.serviceresult.MoveItemsResult;
import com.mycloud.documentservice.serviceresult.UploadDocumentServiceResult;
import com.mycloud.documentservice.services.DocumentService;
import com.mycloud.documentservice.services.ShareDocumentService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.validation.Valid;
import java.util.Map;
import java.util.UUID;

@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class DocumentController {
	private final DocumentService documentService;
	private final ShareDocumentService shareDocumentService;

	@PostMapping(value = "directories/{directoryId}/documents/upload", consumes = {"multipart/form-data"})
	public UploadDocumentServiceResult upload(
			@PathVariable UUID directoryId,
			@Valid @ModelAttribute UploadDocumentDto uploadDocumentDto
	) {
		uploadDocumentDto.setDirectoryId(directoryId);

		return documentService.uploadDocument(uploadDocumentDto);
	}

	@PostMapping("documents/uploadRules")
	public UploadDocumentStatisticsDto getUploadInitInfo(
			@NonNull
			@Valid @RequestBody UploadDocumentInformationDto uploadDocumentInformation) {
		return documentService.getUploadDocumentStatistics(uploadDocumentInformation);
	}

	@GetMapping("documents/{documentId}/{documentName}")
	public ResponseEntity<StreamingResponseBody> getDocument(@PathVariable UUID documentId, @PathVariable String documentName) {
		StreamingResponseBody body = outputStream -> outputStream.write(
				documentService.getDocument(documentId, documentName)
		);

		return ResponseEntity
				.ok()
				.contentType(MediaType.APPLICATION_OCTET_STREAM)
				.body(body);
	}

	@GetMapping("documents/{documentId}/thumbnail")
	public byte[] getThumbnail(
			@PathVariable UUID documentId,
			@RequestParam(name = "h") int height,
			@RequestParam(name = "w") int width) {
		return documentService.getThumbnail(documentId, height, width);
	}

	@GetMapping("documents")
	public CollectionResponse<DocumentDto> documentsByType(
			@RequestParam(name = "t") DocumentType type,
			@RequestParam(name = "u") UUID userID
	) {
		return new CollectionResponse<>(documentService.getDocumentsByType(type, userID));
	}

	@PostMapping("documents/share")
	public Map<String, Boolean> shareDocument(@RequestBody ShareDocumentDto shareDocumentDto) {
		shareDocumentService.share(shareDocumentDto);
		return Map.of("sent", true);
	}

	@DeleteMapping("documents/{documentId}")
	public DeleteEntityResponse delete(@PathVariable UUID documentId) {
		return new DeleteEntityResponse(documentService.deleteDocument(documentId));
	}

	@PostMapping("documents/move")
	public MoveItemsResult moveTo(@RequestBody MoveEntitiesRequest request) {
		return documentService.moveItemsToDirectory(request.getDestinationDirectoryId(), request.getItems());
	}

	@GetMapping("documents/{userId}/latest")
	public CollectionResponse<DocumentDto> latestDocuments(@RequestParam(name = "limit") int limit, @PathVariable int userId) {
		LatestDocumentsFilter filter = new LatestDocumentsFilter(limit, userId);
		return new CollectionResponse<>(documentService.fetchLatestDocuments(filter));
	}

	@GetMapping("documents/search")
	public CollectionResponse<ElasticsearchResultDto<UploadItem>> latestDocuments(@RequestParam(name = "q") String q) {
		return new CollectionResponse<>(documentService.serach(q));
	}

}
