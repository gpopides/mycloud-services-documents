package com.mycloud.documentservice.controllers;

import com.mycloud.documentservice.responses.LatestUploadStatistics;
import com.mycloud.documentservice.serviceresult.UserDocumentsStats;
import com.mycloud.documentservice.services.StatisticsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@AllArgsConstructor
public class StatisticsController {
	private final StatisticsService statisticsService;

	@GetMapping("statistics/documents/latest")
	public LatestUploadStatistics latestUploads(@RequestParam(name = "u") int userId, @RequestParam(name = "l", required = false) Integer limit) {
		return new LatestUploadStatistics(statisticsService.getLatestUploadedDocuments(userId, limit));
	}

	@GetMapping("statistics/general")
	public UserDocumentsStats userFileStatistics(
			@RequestParam(name = "u") int userId,
			@RequestParam(name = "d") UUID directoryId
	) {
		return statisticsService.getUserDocumentsStats(userId, directoryId);
	}
}
