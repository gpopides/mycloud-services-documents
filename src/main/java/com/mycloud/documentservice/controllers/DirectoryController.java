package com.mycloud.documentservice.controllers;

import com.mycloud.documentservice.dto.CreateDirectoryDto;
import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.ExportDirectoryResult;
import com.mycloud.documentservice.dto.FilesystemTree;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.responses.CollectionResponse;
import com.mycloud.documentservice.responses.DeleteEntityResponse;
import com.mycloud.documentservice.serviceresult.CreateDirectoryResult;
import com.mycloud.documentservice.services.DirectoryService;
import lombok.AllArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@CrossOrigin("*")
@RestController
@AllArgsConstructor
public class DirectoryController {
	private final DirectoryService directoryService;

	@GetMapping("users/{userId}/directories")
	public CollectionResponse<Directory> getDirectories(@PathVariable int userId) {
		return new CollectionResponse<>(directoryService.getDirectories(userId));
	}

	@PostMapping("directories")
	public CreateDirectoryResult createDirectory(
			@NonNull @RequestBody CreateDirectoryDto request
	) {
		return directoryService.create(request);
	}

	@GetMapping("directories/fs")
	public FilesystemTree getFilesystemStructure(@RequestParam(name = "userId") int userId) {
		return directoryService.getFilesystemStructure(userId);
	}

	@GetMapping("directories/{directoryId}/documents")
	public CollectionResponse<DocumentDto> documentsOfDirectory(@PathVariable UUID directoryId) {
		return new CollectionResponse<>(directoryService.getDocumentsOfDirectory(directoryId));
	}

	@DeleteMapping("directories/{directoryId}")
	public DeleteEntityResponse delete(@PathVariable UUID directoryId) {
		return new DeleteEntityResponse(directoryService.delete(directoryId));
	}

	@GetMapping("directories/export")
	public ExportDirectoryResult export(@RequestParam int userId) {
		return directoryService.export(userId);
	}


}
