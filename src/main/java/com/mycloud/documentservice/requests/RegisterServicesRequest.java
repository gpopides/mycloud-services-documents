package com.mycloud.documentservice.requests;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class RegisterServicesRequest {
	private List<String> paths;
	private String name;
	private String host;
}
