package com.mycloud.documentservice.elasticrepositories;

import com.mycloud.documentservice.elasticdto.UploadItem;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UploadItemRepository extends
		ElasticsearchRepository<UploadItem, Integer>,
		UploadItemElasticsearchCustomRepository {
}
