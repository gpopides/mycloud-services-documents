package com.mycloud.documentservice.elasticrepositories;

import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.elasticdto.UploadItem;

import java.util.List;

public interface UploadItemElasticsearchCustomRepository {
	List<ElasticsearchResultDto<UploadItem>> searchByName(String name);
}
