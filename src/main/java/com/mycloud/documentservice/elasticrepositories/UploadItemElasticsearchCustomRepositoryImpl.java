package com.mycloud.documentservice.elasticrepositories;

import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.dto.factory.ElasticsearchDtoFactory;
import com.mycloud.documentservice.elasticdto.UploadItem;
import lombok.AllArgsConstructor;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UploadItemElasticsearchCustomRepositoryImpl implements UploadItemElasticsearchCustomRepository {
	ElasticsearchOperations elasticsearchOperations;


	@Override
	public List<ElasticsearchResultDto<UploadItem>> searchByName(String name) {
		QueryBuilder queryBuilder = QueryBuilders.prefixQuery("name", name);
		NativeSearchQuery query = new NativeSearchQueryBuilder()
				.withQuery(queryBuilder)
				.build();

		SearchHits<UploadItem> hits = elasticsearchOperations.search(query, UploadItem.class);

		return ElasticsearchDtoFactory.buildList(hits);
	}
}
