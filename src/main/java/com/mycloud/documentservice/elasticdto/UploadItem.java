package com.mycloud.documentservice.elasticdto;


import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.enums.EntityType;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Document(indexName = "mycloud-uploaded-items")
@Data
public class UploadItem {

	@Id
	private String id;

	@Field(name = "name")
	private String name;

	@Field(name = "path")
	private String path;

	@Field(name = "type")
	private EntityType type;

	public UploadItem(String name, String path, EntityType type) {
		this.name = name;
		this.path = path;
		this.type = type;
	}


	public static UploadItem ofDocument(com.mycloud.documentservice.entities.Document document) {
		return new UploadItem(
				document.getName(),
				document.getPath(),
				EntityType.DOCUMENT
		);
	}

	public static UploadItem ofDirectory(Directory directory) {
		return new UploadItem(
				directory.getName(),
				directory.getPath(),
				EntityType.DIRECTORY
		);
	}
}
