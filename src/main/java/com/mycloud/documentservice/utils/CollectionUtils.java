package com.mycloud.documentservice.utils;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class CollectionUtils {
	public static long iterableSize(Iterable<?> it) {
		return it.spliterator().getExactSizeIfKnown();
	}

	public static <T> Stream<T> iterableToStream(Iterable<T> it, boolean parallel) {
		return StreamSupport.stream(it.spliterator(), parallel);
	}
}
