package com.mycloud.documentservice.utils;

import com.mycloud.documentservice.exceptions.InvalidUploadException;

import java.text.DecimalFormat;
import java.util.Random;

public class GeneralUtils {
	public static String generateRandomString(int length) {
		int leftLimit = 48; // numeral '0'
		int rightLimit = 122; // letter 'z'
		Random random = new Random();

		return random.ints(leftLimit, rightLimit + 1)
				.filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
				.limit(length)
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
				.toString();
	}

	/**
	 * @param chunkId the chunkId concatenated with the chunk index using an underscore
	 * @return the chunk index of the chunkId
	 */
	public static int getFileChunkSequenceIndex(String chunkId) {
		String[] chunkParts = chunkIdParts(chunkId);
		if (chunkParts.length != 2) { // parts must be [someStr, number]
			throw new InvalidUploadException("chunkId is invalid");
		}
		return Integer.parseInt(chunkParts[1]);

	}

	public static String getChunkIdWithoutSequence(String chunkId) {
		return chunkIdParts(chunkId)[0];
	}

	static String[] chunkIdParts(String chunkId) {
		return chunkId.split("_");
	}

	/**
	 * @param b the byte array size
	 * @return the bytes in B/KB/MB/GB format
	 */
	public static String bytesToHumanFormat(long b) {
		if (b <= 0) return "0";
		final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
		int digitGroups = (int) (Math.log10(b) / Math.log10(1024));
		return new DecimalFormat("#,##0.#").format(b / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
}
