package com.mycloud.documentservice.configs;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {
	@Value("${spring.rabbitmq.host}")
	private String rabbitMqHost;

	@Bean
	public Queue documentUploadedQueue() {
		return new Queue("mycloud-documents-service.new.upload", true);
	}

	@Bean
	public Exchange documentUploadExchange() {
		return new FanoutExchange("newUploadExchange");
	}

	@Bean
	public Binding binding(Queue queue, Exchange exchange) {
		return BindingBuilder.bind(queue)
				.to(exchange)
				.with("")
				.noargs();
	}

	@Bean
	public MessageConverter messageConverter() {
		return new Jackson2JsonMessageConverter();
	}


	@Bean(name = "rabbitUploadDocumentQueueTemplate")
	public AmqpTemplate connectionFactory() {
		ConnectionFactory connectionFactory = new CachingConnectionFactory(rabbitMqHost);
		AmqpAdmin admin = new RabbitAdmin(connectionFactory);

		admin.declareBinding(binding(documentUploadedQueue(), documentUploadExchange()));

		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setMessageConverter(messageConverter());
		template.setExchange("newUploadExchange");

		return template;
	}

}
