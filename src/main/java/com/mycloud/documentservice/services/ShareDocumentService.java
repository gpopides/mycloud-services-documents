package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.ShareDocumentDto;

public interface ShareDocumentService {
	void share(ShareDocumentDto shareDocumentDto);
}
