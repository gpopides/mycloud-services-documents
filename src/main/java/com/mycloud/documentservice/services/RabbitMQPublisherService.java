package com.mycloud.documentservice.services;

import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;

public interface RabbitMQPublisherService {
	void publishNewDocument(Document document);

	void publishNewDirectory(Directory directory);
}
