package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;

import java.util.Optional;

public interface DocumentRedisRepository {
	void persistsDocument(UploadDocumentStatisticsDto uploadDocumentStatisticsDto);

	boolean uploadIsValid(UploadDocumentDto uploadDocumentDto);

	Optional<UploadDocumentStatisticsDto> find(String uploadId);

	float calculateUploadPercentage(UploadDocumentDto uploadDocumentDto);

	void deleteUploadId(String uploadId);

	Optional<String> filenameOfUpload(String uploadId);
}
