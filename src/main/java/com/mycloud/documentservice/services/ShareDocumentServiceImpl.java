package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.SendMethod;
import com.mycloud.documentservice.dto.ShareDocumentDto;
import com.mycloud.documentservice.dto.ShareReceiver;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.repositories.DocumentRepository;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
@AllArgsConstructor
public class ShareDocumentServiceImpl implements ShareDocumentService {
	private final JavaMailSender javaMailSender;
	private final DocumentRepository documentRepository;
	private final long MAX_ATTACHMENT_SIZE = (long) Math.pow(1024, 2); // 10M

	@Async
	@Override
	public void share(ShareDocumentDto shareDocumentDto) {
		SendMethod shareMethod = shareDocumentDto.getMethod();
		boolean canShare = shareDocumentDto.getReceiver().receiverInfoIsValid(shareMethod);
		if (shareMethod == SendMethod.EMAIL && canShare) {

			documentRepository.findById(shareDocumentDto.getDocumentId())
					.ifPresentOrElse(document -> sendEmail(shareDocumentDto.getReceiver(), document),
							() -> {
								throw new EntityNotFoundException("document not found");
							}
					);

		}
	}

	private void sendEmail(ShareReceiver receiver, Document document) {
		if (document.getSize() <= MAX_ATTACHMENT_SIZE && DocumentDto.canBeSentThroughEmail(document)) {
			try {
				MimeMessage mimeMessage = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

				String from = "mycloud@somecloud.com";

				helper.setFrom(from);
				helper.setTo(receiver.getEmail());
				helper.setSubject("MYCLOUD");
				String text = String.format("This email was sent by %s to share a file.  Look at attachments", from);
				helper.setText(text);

				Path filePath = Path.of(document.getPath());

				helper.addAttachment(document.getName(), new ByteArrayResource(Files.readAllBytes(filePath)));

				javaMailSender.send(mimeMessage);
			} catch (MessagingException | IOException e) {
				e.printStackTrace();
			}
		}
	}

}
