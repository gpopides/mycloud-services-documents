package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.serviceresult.UserDocumentsStats;
import com.mycloud.documentservice.utils.CollectionUtils;
import com.mycloud.documentservice.utils.GeneralUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {
	private final DocumentRepository documentRepository;
	private final DirectoryRepository directoryRepository;


	@Override
	public Iterable<DocumentDto> getLatestUploadedDocuments(int userId, Integer limit) {
		final int DEFAULT_LIMIT = 10;

		int _limit = Optional.ofNullable(limit).isPresent()
				? limit
				: DEFAULT_LIMIT;

		return CollectionUtils.iterableToStream(documentRepository.findAllByUserId(userId), false)
				.sorted(Document.dateComparator())
				.limit(_limit)
				.map(DocumentDto::ofDocument)
				.collect(Collectors.toList());
	}

	@Override
	public UserDocumentsStats getUserDocumentsStats(int userId, UUID directoryId) {
		UserDocumentsStats userDocumentsStats = new UserDocumentsStats();

		directoryRepository.findById(directoryId).ifPresentOrElse(directory -> {
			userDocumentsStats.setNumberOfDocuments(directory.getDocuments().size());
			userDocumentsStats.setNumberOfSubDirectories(directory.getSubDirectories().size());

			long filesSize = directory.getDocuments().stream().mapToLong(Document::getSize).sum();
			userDocumentsStats.setTotalSize(GeneralUtils.bytesToHumanFormat(filesSize));
		}, () -> {
			throw new EntityNotFoundException("directory not found");
		});

		return userDocumentsStats;
	}
}
