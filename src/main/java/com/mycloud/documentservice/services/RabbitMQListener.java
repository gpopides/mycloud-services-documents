package com.mycloud.documentservice.services;

import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class RabbitMQListener {
	private final DirectoryService directoryService;

	@RabbitListener(queues = "mycloud-auth-service.new.user")
	public void receive(String newUserId) {
		int userId = Integer.parseInt(newUserId);
		directoryService.createRootDirectory(userId);
	}

}

