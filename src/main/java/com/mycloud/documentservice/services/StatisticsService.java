package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.serviceresult.UserDocumentsStats;

import java.util.UUID;

public interface StatisticsService {
	Iterable<DocumentDto> getLatestUploadedDocuments(int uuid, Integer limit);

	UserDocumentsStats getUserDocumentsStats(int userId, UUID directoryId);
}
