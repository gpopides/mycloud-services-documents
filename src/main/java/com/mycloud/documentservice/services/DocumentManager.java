package com.mycloud.documentservice.services;

import com.mycloud.documentservice.beans.FileManager;
import com.mycloud.documentservice.dto.SaveFileResult;
import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import org.apache.tomcat.util.http.fileupload.InvalidFileNameException;
import org.springframework.web.multipart.MultipartFile;

import java.util.function.Consumer;

public interface DocumentManager extends FileManager {
	void saveFile(UploadDocumentDto uploadDocumentDto, String pathToSave, Consumer<SaveFileResult> consumer);

	DocumentType determineDocumentFile(MultipartFile file, String uploadId);

	boolean saveChunk(UploadDocumentDto uploadDocumentDto);

	boolean isFinalChunk(int chunkIdSequence, String uploadId);

	boolean isFullUpload(String uploadId);

	void mergeChunks(String uploadId, String destinationDirectory, Consumer<SaveFileResult> consumer);

	byte[] fileContent(String path);

	boolean uploadIsValid(UploadDocumentDto uploadDocumentDto);

	void deleteUploadId(String uploadId);

	float calculateUploadPercentage(UploadDocumentDto uploadDocumentDto);

	void persistsDocument(UploadDocumentStatisticsDto uploadDocumentStatisticsDto);

	boolean documentSupportsThumbnailGeneration(DocumentType type);

	void moveAndUpdateAttributes(Document document, Directory destination);

	void moveAndUpdateAttributes(Directory directory, Directory destination);

	static String extension(String filename) {
		int lastDotIndex = filename.lastIndexOf(".");

		if (lastDotIndex == -1) {
			throw new InvalidFileNameException(filename, "Invalid filename");
		}

		return filename.substring(lastDotIndex + 1);
	}

}
