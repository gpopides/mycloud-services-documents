package com.mycloud.documentservice.services;

import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.rabbitmqmessage.NewEntityCreatedMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class RabbitMQPublisherServiceImpl implements RabbitMQPublisherService {
	@Qualifier("rabbitUploadDocumentQueueTemplate")
	private final AmqpTemplate rabbitUploadDocumentQueueTemplate;

	@Override
	public void publishNewDocument(Document document) {
		NewEntityCreatedMessage message = NewEntityCreatedMessage.of(document);
		rabbitUploadDocumentQueueTemplate.convertAndSend(message);
	}

	@Override
	public void publishNewDirectory(Directory directory) {
		NewEntityCreatedMessage message = NewEntityCreatedMessage.of(directory);
		rabbitUploadDocumentQueueTemplate.convertAndSend(message);
	}

}
