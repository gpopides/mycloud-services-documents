package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.SynchronizeResultDto;

public interface FilesystemService {
	SynchronizeResultDto synchronize(String clientId, int userId);
}
