package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Optional;


@Service
public class DocumentRedisRepositoryImpl implements DocumentRedisRepository {
	private final HashOperations<String, String, UploadDocumentStatisticsDto> redisHashOperations;
	private final String REDIS_KEY = "documents";

	public DocumentRedisRepositoryImpl(RedisTemplate<String, UploadDocumentStatisticsDto> template) {
		this.redisHashOperations = template.opsForHash();
	}

	/**
	 * Persist the documentStatisticsDto temporarily so we can
	 * validate upload requests when they are sent to the upload method
	 *
	 * @param uploadDocumentStatisticsDto uploadDocumentStatisticsDto
	 */
	@Override
	public void persistsDocument(UploadDocumentStatisticsDto uploadDocumentStatisticsDto) {
		redisHashOperations.put(
				REDIS_KEY,
				uploadDocumentStatisticsDto.getUploadId(),
				uploadDocumentStatisticsDto
		);
	}

	@Override
	public boolean uploadIsValid(UploadDocumentDto uploadDocumentDto) {
		boolean isValid;

		Optional<UploadDocumentStatisticsDto> documentStatisticsDto = find(uploadDocumentDto.getUploadId());

		if (documentStatisticsDto.isEmpty()) {
			isValid = false;
		} else {
			UploadDocumentStatisticsDto _documentStatisticsDto = documentStatisticsDto.get();


			// the UploadDocumentStatisticsDto  is valid if its sequence number is smaller than the count of total chunks
			// and it's chunk size is smaller than the chunk size the server determined in the *init* call.
			isValid = uploadDocumentDto.chunkIdSequence() <= _documentStatisticsDto.getChunksCount()
					&& uploadDocumentDto.getChunk().getSize() <= _documentStatisticsDto.getChunkSize()
					&& uploadDocumentDto.getUploadIdOfChunkId().equals(_documentStatisticsDto.getUploadId());
		}

		return isValid;
	}

	@Override
	public Optional<UploadDocumentStatisticsDto> find(String uploadId) {
		return Optional.ofNullable(redisHashOperations.get(REDIS_KEY, uploadId));
	}

	@Override
	public float calculateUploadPercentage(UploadDocumentDto uploadDocumentDto) {
		Optional<UploadDocumentStatisticsDto> documentStatisticsDto = find(uploadDocumentDto.getUploadId());
		float percentage = 0f;

		if (documentStatisticsDto.isPresent()) {
			percentage = (float) uploadDocumentDto.chunkIdSequence() * uploadDocumentDto.getChunk().getSize() / documentStatisticsDto.get().getTotalSize();
		}
		DecimalFormat df = new DecimalFormat("##.##");
		return Float.parseFloat(df.format(percentage));
	}

	@Override
	public void deleteUploadId(String uploadId) {
		redisHashOperations.delete(REDIS_KEY, uploadId);
	}

	@Override
	public Optional<String> filenameOfUpload(String uploadId) {
		Optional<UploadDocumentStatisticsDto> documentStatisticsDto = find(uploadId);

		return Optional.ofNullable(documentStatisticsDto.get().getFilename());
	}
}
