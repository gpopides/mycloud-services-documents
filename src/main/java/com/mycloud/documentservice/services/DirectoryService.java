package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.CreateDirectoryDto;
import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.ExportDirectoryResult;
import com.mycloud.documentservice.dto.FilesystemTree;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.serviceresult.CreateDirectoryResult;

import java.util.UUID;

public interface DirectoryService {
	Iterable<Directory> getDirectories(int userId);

	CreateDirectoryResult create(CreateDirectoryDto dto);

	FilesystemTree getFilesystemStructure(int userId);

	Iterable<DocumentDto> getDocumentsOfDirectory(UUID directoryId);

	boolean delete(UUID directoryId);

	void createRootDirectory(int userId);

	ExportDirectoryResult export(int userId);

}
