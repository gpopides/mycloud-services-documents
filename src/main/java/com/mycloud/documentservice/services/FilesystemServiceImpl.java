package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.SynchronizeResultDto;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.repositories.FilesystemRedisService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class FilesystemServiceImpl implements FilesystemService {
	private final DirectoryRepository directoryRepository;
	private final DocumentRepository documentRepository;
	private final FilesystemRedisService filesystemRedisService;

	@Override
	public SynchronizeResultDto synchronize(String clientId, int userId) {
		Optional<LocalDateTime> lastSync = filesystemRedisService.getLatestSynchronizationTimestamp(clientId);
		SynchronizeResultDto result = new SynchronizeResultDto();

		List<UUID> documentIds = new ArrayList<>();
		List<UUID> directoryIds = new ArrayList<>();

		lastSync.ifPresentOrElse(ls -> {
			directoryRepository.findAllLatestBasedOnTimestamp(ls, userId).forEach(directoryIds::add);
			documentRepository.findAllLatestBasedOnTimestamp(ls, userId).forEach(documentIds::add);
		}, () -> {
			directoryRepository.findAllByUserId(userId).forEach(d -> directoryIds.add(d.getId()));
			documentRepository.findAllByUserId(userId).forEach(d -> documentIds.add(d.getId()));
		});

		filesystemRedisService.updateSynchronizationTimestamp(clientId, LocalDateTime.now());

		result.setDocumentIds(documentIds);
		result.setDirectoryIds(directoryIds);

		return result;
	}
}
