package com.mycloud.documentservice.services;

import com.mycloud.documentservice.beans.DirectoryManager;
import com.mycloud.documentservice.beans.FileManager;
import com.mycloud.documentservice.dto.SaveFileResult;
import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.exceptions.InvalidUploadException;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.utils.GeneralUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.InvalidFileNameException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

@Service
@AllArgsConstructor
@Slf4j
public class DocumentManagerImpl implements DocumentManager {
	private final DocumentRepository documentRepository;
	private final DirectoryManager directoryManager;


	@Override
	public void saveFile(UploadDocumentDto uploadDocumentDto, String directoryPathToSaveIn, Consumer<SaveFileResult> consumer) {
		Path destination = Paths.get(directoryPathToSaveIn, uploadDocumentDto.getFilename());
		SaveFileResult result = new SaveFileResult();

		try {
			uploadDocumentDto.getChunk().transferTo(destination);
			result.setSaved(true);
			result.setPath(destination);
			result.setSize(Files.size(destination));
		} catch (IOException e) {
			e.printStackTrace();
			result.setSaved(false);
		}

		consumer.accept(result);
	}

	@Override
	public DocumentType determineDocumentFile(MultipartFile file, String uploadId) {
		Optional<String> filename = documentRepository.filenameOfUpload(uploadId);
		DocumentType documentType;

		Map<DocumentType, Set<String>> validExtensionsPerType = Map.of(
				DocumentType.IMAGE, Set.of("png", "jpg", "jpeg"),
				DocumentType.VIDEO, Set.of("mp4"),
				DocumentType.TEXT_FILE, this.getCodeSourceCodeExtensions()
		);

		if (filename.isPresent()) {

			String fileExtension = DocumentManager.extension(filename.get());

			if (validExtensionsPerType.get(DocumentType.IMAGE).contains(fileExtension)) {
				documentType = DocumentType.IMAGE;
			} else if (validExtensionsPerType.get(DocumentType.TEXT_FILE).contains(fileExtension)) {
				documentType = DocumentType.TEXT_FILE;
			} else {
				documentType = DocumentType.VIDEO;
			}

		} else {
			throw new InvalidFileNameException("uploadedFle", "Invalid filename");

		}
		return documentType;
	}

	@Override
	public boolean saveChunk(UploadDocumentDto uploadDocumentDto) {
		boolean saved;
		try {
			Path uploadPath = directoryManager.constructTmpUploadPath(
					uploadDocumentDto.getUploadId(),
					uploadDocumentDto.getChunkId()
			);
			uploadDocumentDto.getChunk().transferTo(uploadPath);
			saved = true;
		} catch (IOException e) {
			e.printStackTrace();
			saved = false;
		}

		return saved;
	}


	@Override
	public boolean isFinalChunk(int chunkIdSequence, String uploadId) {
		Optional<UploadDocumentStatisticsDto> uploadDocumentStatisticsDto = documentRepository.find(uploadId);

		return uploadDocumentStatisticsDto.isPresent()
				&& chunkIdSequence == uploadDocumentStatisticsDto.get().getChunksCount();
	}

	@Override
	public boolean isFullUpload(String uploadId) {
		Optional<UploadDocumentStatisticsDto> uploadDocumentStatisticsDto = documentRepository.find(uploadId);

		return uploadDocumentStatisticsDto.isPresent() && uploadDocumentStatisticsDto.get().getChunksCount() == 1;
	}

	@Override
	public void mergeChunks(String uploadId, String destinationDirectory, Consumer<SaveFileResult> fileResultConsumer) {
		SaveFileResult result = new SaveFileResult();
		Optional<UploadDocumentStatisticsDto> uploadDocumentStatisticsDto = documentRepository.find(uploadId);
		Path directoryWithChunks = directoryManager.constructTmpUploadPath(uploadId);

		if (uploadDocumentStatisticsDto.isEmpty()) throw new InvalidUploadException("upload is invalid");

		Path mergedFilePath = Paths.get(destinationDirectory, uploadDocumentStatisticsDto.get().getFilename());

		File mergedFile = new File(mergedFilePath.toString());

		try (FileOutputStream fileOutputStream = new FileOutputStream(mergedFile)) {
			Files.list(directoryWithChunks)
					.sorted((p1, p2) -> {
						// first sort the files based the chunkid sequence
						int chunkIndex1 = GeneralUtils.getFileChunkSequenceIndex(p1.getFileName().toString());
						int chunkIndex2 = GeneralUtils.getFileChunkSequenceIndex(p2.getFileName().toString());

						return Integer.compare(chunkIndex1, chunkIndex2);
					})
					.forEach(p -> {
						try {
							fileOutputStream.write(Files.readAllBytes(p));
						} catch (IOException e) {
							e.printStackTrace();
							throw new InvalidUploadException("failed to save last part");
						}
					});
			directoryManager.deleteNonEmptyDirectory(directoryWithChunks);
			result.setSaved(true);
			result.setPath(mergedFilePath);
			result.setSize(Files.size(mergedFilePath));

			fileResultConsumer.accept(result);
		} catch (IOException e) {
			e.printStackTrace();
			throw new InvalidUploadException("failed to save last part");
		}
	}

	@Override
	public byte[] fileContent(String path) {
		try {
			return Files.readAllBytes(Path.of(path));
		} catch (IOException e) {
			e.printStackTrace();
			// TODO: change exception
			throw new EntityNotFoundException();
		}
	}

	@Override
	public boolean uploadIsValid(UploadDocumentDto uploadDocumentDto) {
		return documentRepository.uploadIsValid(uploadDocumentDto);
	}

	@Override
	public void deleteUploadId(String uploadId) {
		documentRepository.deleteUploadId(uploadId);
	}

	@Override
	public float calculateUploadPercentage(UploadDocumentDto uploadDocumentDto) {
		return documentRepository.calculateUploadPercentage(uploadDocumentDto);
	}

	@Override
	public void persistsDocument(UploadDocumentStatisticsDto uploadDocumentStatisticsDto) {
		documentRepository.persistsDocument(uploadDocumentStatisticsDto);
	}

	@Override
	public void moveAndUpdateAttributes(Document document, Directory destination) {
		FileManager.moveTo(document.getPath(), destination.getPath());

		String newPath = FileManager.getDestinationFullPath(
				document.getPath(),
				destination.getPath()
		).toString();
		document.setDirectory(destination);
		document.setPath(newPath);
	}

	@Override
	public void moveAndUpdateAttributes(Directory directory, Directory destination) {
		directoryManager.moveAndUpdateAttributes(directory, destination);
	}

	private Set<String> getCodeSourceCodeExtensions() {
		return Set.of(
				"c", "cc", "class", "clj", "cpp", "cs",
				"cxx", "el", "go", "h", "java", "lua",
				"m", "m4", "php", "pl", "po", "py", "rb",
				"rs", "sh", "swift", "vb", "vcxproj",
				"xcodeproj", "xml", "diff", "patch",
				"html", "js", "txt"
		);
	}

	@Override
	public boolean documentSupportsThumbnailGeneration(DocumentType type) {
		return type == DocumentType.IMAGE;
	}
}