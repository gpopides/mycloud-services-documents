package com.mycloud.documentservice.services;

import com.mycloud.documentservice.beans.DirectoryManager;
import com.mycloud.documentservice.dto.CreateDirectoryDto;
import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.ExportDirectoryResult;
import com.mycloud.documentservice.dto.FilesystemTree;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.exceptions.CreateDirectoryException;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.serviceresult.CreateDirectoryResult;
import com.mycloud.documentservice.utils.CollectionUtils;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DirectoryServiceImpl implements DirectoryService {
	private final DirectoryRepository directoryRepository;
	private final DirectoryManager directoryManager;
	private final FilesystemResourceExtraActionService filesystemResourceExtraActionService;

	@Override
	public Iterable<Directory> getDirectories(int userId) {
		return directoryRepository.findAllByUserId(userId);
	}

	@Override
	public CreateDirectoryResult create(CreateDirectoryDto dto) {
		CreateDirectoryResult result = new CreateDirectoryResult();

		List<Directory> directoriesToBeCreated = getDirectoriesListForCreation(dto.getPath(), dto.getUserId())
				.stream()
				.map(dp -> {
							Directory newDir;
							try {
								Files.createDirectories(dp);
								newDir = new Directory(
										dp.toString(),
										dp.getFileName().toString(),
										dto.getUserId()
								);
							} catch (IOException e) {
								e.printStackTrace();
								result.setCreated(false);
								result.setStatus(HttpStatus.BAD_REQUEST);
								result.setError("Could not create directory");
								throw new CreateDirectoryException(result);
							}
							return newDir;
						}
				).collect(Collectors.toList());

		Iterable<Directory> createdDirectories = directoryRepository.saveAll(directoriesToBeCreated);
		directoryRepository.flush();

		setParentForEachDirectory(directoriesToBeCreated, dto.getUserId());

		result.setCreated(CollectionUtils.iterableSize(createdDirectories) == directoriesToBeCreated.size());

		return result;
	}

	public FilesystemTree getFilesystemStructure(int userId) {
		FilesystemTree fs = new FilesystemTree();
		Optional<Directory> rootDirectory = directoryRepository.getRootDirectory(userId);

		rootDirectory.ifPresentOrElse(fs::setFs, () -> {
			throw new EntityNotFoundException("Root directory of user does not exist");
		});

		return fs;
	}

	/**
	 * @param directoryId the uuid of the directory
	 * @return an object that holds a collection of documents
	 */
	@Override
	public Iterable<DocumentDto> getDocumentsOfDirectory(UUID directoryId) {
		List<DocumentDto> documents = new ArrayList<>();

		directoryRepository
				.findById(directoryId)
				.ifPresentOrElse(directory -> {
					CollectionUtils.iterableToStream(directory.getDocuments(), false)
							.map(DocumentDto::ofDocument)
							.forEach(documents::add);
				}, () -> {
					throw new EntityNotFoundException();
				});
		return documents;
	}


	@Override
	public boolean delete(UUID directoryId) {
		directoryRepository.findById(directoryId)
				.ifPresentOrElse(directory -> {
					Optional<Iterable<Directory>> childrenDirectories = Optional.ofNullable(directoryRepository.getChildrenDirectories(directory.getId()));

					childrenDirectories.ifPresent(cd -> cd.forEach(
							childDirectory -> {
								directoryManager.deleteNonEmptyDirectory(Path.of(childDirectory.getPath()));
								directoryRepository.delete(childDirectory);
							}
					));

					directoryManager.deleteNonEmptyDirectory(Path.of(directory.getPath()));
					directoryRepository.delete(directory);
				}, () -> {
					throw new EntityNotFoundException();
				});

		return true;
	}

	@Override
	public void createRootDirectory(int userId) {
		CreateDirectoryResult result = directoryManager.createRootDirectory(userId);

		if (result.isCreated()) {
			Directory rootDir = new Directory(
					result.getPath(),
					"root",
					userId
			);
			directoryRepository.save(rootDir);
		}
	}


	/**
	 * Creates a Directory object for the path that the user has sent
	 * and for each of it's non existing parents.
	 *
	 * @param fullNewDirectoryPath the path that the user sent to be created
	 * @return directoriesList the new directories to be created
	 */
	private List<Path> getDirectoriesListForCreation(String fullNewDirectoryPath, int userId) {
		List<String> directoryNames = Arrays
				.stream(fullNewDirectoryPath.split("/"))
				.filter(dirName -> !dirName.isBlank() && !dirName.isEmpty())
				.collect(Collectors.toList());
		List<Path> directoryPathsToBeCreated = new ArrayList<>();

		Path directoryPath = directoryManager.getBaseUserPath(userId);

		for (String directory : directoryNames) {
			directoryPath = Paths.get(directoryPath.toString(), directory); // append the directory name

			if (!Files.exists(directoryPath)) {
				directoryPathsToBeCreated.add(directoryPath);
			}
		}


		return directoryPathsToBeCreated;
	}

	/**
	 * This method tries to assign a parent (directory) to each one of the directories that will be saved in the
	 * database.
	 *
	 * @param recentlyCreatedDirectories Collection of directories entities that must be saved in the database.
	 */
	private void setParentForEachDirectory(Iterable<Directory> recentlyCreatedDirectories, int userId) {
		// create a list with string paths that are the parent directories of the recently created directories
		List<String> parentPathsList = CollectionUtils.iterableToStream(recentlyCreatedDirectories, false)
				.map(directoryManager::getParentDirectory)
				.collect(Collectors.toList());

		// try to find the matching directory entities in the database and create a map with them
		// where the keys are their paths and values the directories themselves.
		Map<String, Directory> possibleParentDirectoriesMap = CollectionUtils
				.iterableToStream(directoryRepository.findByPathIn(parentPathsList), false)
				.collect(Collectors.toMap(Directory::getPath, dir -> dir));


		// loop through the new directories
		recentlyCreatedDirectories.forEach(directory -> {
			String recentDirectoryParentPath = directoryManager.getParentDirectory(directory);

			// check if we have it's parent directory already available and loaded
			// and set it if we do
			if (possibleParentDirectoriesMap.containsKey(recentDirectoryParentPath)) {
				directory.setParentDir(possibleParentDirectoriesMap.get(recentDirectoryParentPath));
			} else {
				final String parentDirectoryName = directoryManager.parentIsRoot(recentDirectoryParentPath)
						? "root"
						: directoryManager.directoryName(recentDirectoryParentPath);

				// otherwise, create a new record in the db, and assign the new parent.
				Directory newParentDirectory = new Directory(
						recentDirectoryParentPath,
						parentDirectoryName,
						userId
				);
				directoryRepository.save(newParentDirectory);
				directory.setParentDir(newParentDirectory);
			}

			directoryRepository.save(directory);
			filesystemResourceExtraActionService.publishToThirdPartyServices(directory, Directory.class);
		});
	}

	@Override
	public ExportDirectoryResult export(int userId) {
		ExportDirectoryResult result = new ExportDirectoryResult();

		directoryRepository.getRootDirectory(userId).ifPresentOrElse(directory -> {
			directoryManager.zipDirectory(directory, userId);
			result.setStarted(true);
		}, () -> {
			result.setStarted(false);
			result.setError(String.format("root directory for user %s does not exist", userId));
		});

		return result;
	}
}
