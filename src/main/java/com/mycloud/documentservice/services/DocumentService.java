package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.dto.LatestDocumentsFilter;
import com.mycloud.documentservice.dto.MoveEntityDto;
import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentInformationDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;
import com.mycloud.documentservice.elasticdto.UploadItem;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.serviceresult.MoveItemsResult;
import com.mycloud.documentservice.serviceresult.UploadDocumentServiceResult;

import java.util.List;
import java.util.UUID;


public interface DocumentService {
	UploadDocumentServiceResult uploadDocument(UploadDocumentDto uploadDocumentDto);

	UploadDocumentStatisticsDto getUploadDocumentStatistics(UploadDocumentInformationDto uploadDocumentInformationDto);

	byte[] getDocument(UUID documentId, String documentName);

	byte[] getThumbnail(UUID documentId, int height, int width);

	Iterable<DocumentDto> getDocumentsByType(DocumentType type, UUID userId);

	boolean deleteDocument(UUID documentId);

	MoveItemsResult moveItemsToDirectory(UUID destinationDirectoryId, List<MoveEntityDto> itemsToMove);

	Iterable<DocumentDto> fetchLatestDocuments(LatestDocumentsFilter filter);

	List<ElasticsearchResultDto<UploadItem>> serach(String name);
}
