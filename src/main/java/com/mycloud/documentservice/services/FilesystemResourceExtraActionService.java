package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.elasticdto.UploadItem;

import java.util.List;

public interface FilesystemResourceExtraActionService {
	<T> void publishToThirdPartyServices(T item, Class<T> cls);

	List<ElasticsearchResultDto<UploadItem>> searchInElastic(String name);
}
