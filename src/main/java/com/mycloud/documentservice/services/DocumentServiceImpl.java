package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.DocumentDto;
import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.dto.LatestDocumentsFilter;
import com.mycloud.documentservice.dto.MoveEntityDto;
import com.mycloud.documentservice.dto.SaveFileResult;
import com.mycloud.documentservice.dto.UploadDocumentDto;
import com.mycloud.documentservice.dto.UploadDocumentInformationDto;
import com.mycloud.documentservice.dto.UploadDocumentStatisticsDto;
import com.mycloud.documentservice.elasticdto.UploadItem;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.enums.EntityType;
import com.mycloud.documentservice.exceptions.InvalidUploadException;
import com.mycloud.documentservice.exceptions.UnsupportedThumbnailDocumentException;
import com.mycloud.documentservice.repositories.DirectoryRepository;
import com.mycloud.documentservice.repositories.DocumentRepository;
import com.mycloud.documentservice.serviceresult.MoveItemsResult;
import com.mycloud.documentservice.serviceresult.UploadDocumentServiceResult;
import com.mycloud.documentservice.utils.CollectionUtils;
import com.mycloud.documentservice.utils.GeneralUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.persistence.EntityNotFoundException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class DocumentServiceImpl implements DocumentService {
	private final DocumentRepository documentRepository;
	private final DirectoryRepository directoryRepository;
	private final DocumentManager documentManager;
	private final FilesystemResourceExtraActionService filesystemResourceExtraActionService;

	@Override
	public UploadDocumentServiceResult uploadDocument(UploadDocumentDto uploadDocumentDto) {
		UploadDocumentServiceResult result = new UploadDocumentServiceResult(false);

		if (!documentManager.uploadIsValid(uploadDocumentDto) || !directoryRepository.existsById(uploadDocumentDto.getDirectoryId())) {
			String msg = String.format("Upload data for id %s is not correct", uploadDocumentDto.getUploadId());
			throw new InvalidUploadException(msg);
		}

		// full upload means that the file sent is smaller than the MAX_SIZE
		if (documentManager.isFullUpload(uploadDocumentDto.getUploadId())) {
			directoryRepository.findById(uploadDocumentDto.getDirectoryId()).ifPresentOrElse(directory -> {
				documentManager.saveFile(
						uploadDocumentDto,
						directory.getPath(),
						saveFileConsumer(uploadDocumentDto, result, directory)
				);

				documentManager.deleteUploadId(uploadDocumentDto.getUploadId());
			}, () -> {
				throw new EntityNotFoundException();
			});
		} else {
			boolean saved = documentManager.saveChunk(uploadDocumentDto);
			result.setSaved(saved);

			if (saved && !documentManager.isFinalChunk(uploadDocumentDto.chunkIdSequence(), uploadDocumentDto.getUploadId())) {
				result.setPercentage(documentManager.calculateUploadPercentage(uploadDocumentDto));
			} else {
				// final chunk, merge all the chunks and save to the db
				directoryRepository.findById(uploadDocumentDto.getDirectoryId()).ifPresentOrElse(directory -> documentManager.mergeChunks(
						uploadDocumentDto.getUploadId(),
						directory.getPath(),
						saveFileConsumer(uploadDocumentDto, result, directory)
				), () -> {
					throw new EntityNotFoundException();
				});

				documentManager.deleteUploadId(uploadDocumentDto.getUploadId());
			}
		}

		return result;
	}

	/**
	 * Creates an object that lets the client know what he should send
	 * and how, in order to upload successfully a Document.
	 *
	 * @param uploadDocumentInformationDto UploadDocumentInformationDto
	 * @return UploadDocumentStatisticsDto
	 */
	@Override
	public UploadDocumentStatisticsDto getUploadDocumentStatistics(UploadDocumentInformationDto uploadDocumentInformationDto) {
		final long MAX_CHUNK_SIZE = 1024 * 1024 * 2; // 2MB

		long chunkSize;
		long chunks;

		if (!directoryRepository.existsById(uploadDocumentInformationDto.getDirectoryId())) {
			throw new InvalidUploadException("invalidDirectory");
		}

		if (uploadDocumentInformationDto.getFilesize() <= MAX_CHUNK_SIZE) {
			chunks = 1;
			chunkSize = uploadDocumentInformationDto.getFilesize();
		} else {
			chunks = (long) Math.ceil((double) uploadDocumentInformationDto.getFilesize() / MAX_CHUNK_SIZE);
			chunkSize = MAX_CHUNK_SIZE;
		}

		String uploadId = GeneralUtils.generateRandomString(10);

		UploadDocumentStatisticsDto uploadDocumentStatisticsDto = new UploadDocumentStatisticsDto(
				chunks,
				uploadId,
				chunkSize,
				uploadDocumentInformationDto.getFilename(),
				uploadDocumentInformationDto.getFilesize()
		);

		documentManager.persistsDocument(uploadDocumentStatisticsDto);

		return uploadDocumentStatisticsDto;
	}

	/**
	 * @param documentId the documentId
	 * @return a byte stream which is a thumbnail of the document.
	 */
	public byte[] getThumbnail(UUID documentId, int height, int width) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		documentRepository.findById(documentId).ifPresentOrElse((document) -> {
			if (documentManager.documentSupportsThumbnailGeneration(document.getDocumentType())) {
				try (
						InputStream is = new FileInputStream(document.getPath())
				) {
					BufferedImage img = ImageIO.read(is);

					// if the actual image is within the thumbnail bounds
					// just the return the original bytestream
					if (img.getHeight() <= height && img.getWidth() <= width) {
						baos.write(is.read());
					} else {
						BufferedImage thumbnail = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

						Graphics g = thumbnail.createGraphics();
						g.drawImage(img, 0, 0, width, height, null);
						g.dispose();
						ImageIO.write(thumbnail, "png", baos);
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				String errorMessage = String.format("%s does not support thumbnails", document.getDocumentType());
				throw new UnsupportedThumbnailDocumentException(errorMessage);
			}

		}, () -> {
			throw new EntityNotFoundException("documentNotFound");
		});

		return baos.toByteArray();
	}

	/**
	 * @param type   the type of the document
	 * @param userID the id of the user where the documents belong to
	 * @return all the documents that are of the given type
	 */
	@Override
	public Iterable<DocumentDto> getDocumentsByType(DocumentType type, UUID userID) {
		return CollectionUtils.iterableToStream(documentRepository.findDocumentsOfUserByType(type, userID), false)
				.map(DocumentDto::ofDocument)
				.collect(Collectors.toList());
	}

	/**
	 * @param documentId the document id
	 * @return the content of the document.
	 */
	@Override
	public byte[] getDocument(UUID documentId, String documentName) {
		Optional<Document> document = documentRepository.findById(documentId);

		if (document.isEmpty() || !document.get().getName().equals(documentName)) {
			throw new EntityNotFoundException("document not found");
		}

		return documentManager.fileContent(document.get().getPath());

	}

	@Override
	public boolean deleteDocument(UUID documentId) {
		boolean deleted = false;
		Optional<Document> document = documentRepository.findById(documentId);

		if (document.isPresent()) {
			documentRepository.delete(document.get());
			try {
				Files.deleteIfExists(Path.of(document.get().getPath()));
				deleted = true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return deleted;
	}

	/**
	 * Moves the items from one directory to another one.
	 *
	 * @param itemsToMove a list of {@link MoveEntityDto} where
	 *                    each one contains the id of the item and it's type(Document, Directory)
	 */
	@Override
	public MoveItemsResult moveItemsToDirectory(UUID destinationDirectoryId, List<MoveEntityDto> itemsToMove) {
		MoveItemsResult result = new MoveItemsResult();
		directoryRepository.findById(destinationDirectoryId).ifPresentOrElse(destinationDirectory -> {
			List<UUID> documentIdsToBeUpdated = new ArrayList<>();
			List<UUID> directoryIdsToBeUpdated = new ArrayList<>();

			itemsToMove.forEach(item -> {
				if (item.getEntityType() == EntityType.DOCUMENT) {
					documentIdsToBeUpdated.add(item.getId());
				} else {
					directoryIdsToBeUpdated.add(item.getId());
				}
			});

			List<Document> updatedDocuments = moveItemsToDestination(
					documentRepository.findAllById(documentIdsToBeUpdated),
					destinationDirectory,
					EntityType.DOCUMENT
			);
			List<Directory> updatedDirectories = moveItemsToDestination(
					directoryRepository.findAllById(directoryIdsToBeUpdated),
					destinationDirectory,
					EntityType.DIRECTORY
			);
			directoryRepository.saveAll(updatedDirectories);
			documentRepository.saveAll(updatedDocuments);

			// all items where moved
			if (itemsToMove.size() == (updatedDocuments.size() + updatedDirectories.size())) {
				result.setMoved(true);
			} else {
				// otherwise something went wrong
				result.setMoved(false);
				result.setError("Some items could not be moved");
			}
		}, () -> {
			log.error(String.format("Directory with id %s does not exist while moving files", destinationDirectoryId.toString()));
			throw new EntityNotFoundException("destination directory not found");
		});
		return result;
	}

	@Override
	public Iterable<DocumentDto> fetchLatestDocuments(LatestDocumentsFilter filter) {
		return documentRepository.fetchLatestDocuments(
				filter.getUserID(),
				filter.getLimit()
		)
				.stream()
				.map(DocumentDto::ofDocument)
				.collect(Collectors.toList());
	}

	@Override
	public List<ElasticsearchResultDto<UploadItem>> serach(String name) {
		return filesystemResourceExtraActionService.searchInElastic(name);
	}

	private <T> List<T> moveItemsToDestination(List<T> items, Directory destination, EntityType type) {
		return items
				.stream()
				.peek(item -> {
					if (type == EntityType.DOCUMENT) {
						documentManager.moveAndUpdateAttributes((Document) item, destination);
					} else {
						documentManager.moveAndUpdateAttributes((Directory) item, destination);
					}
				})
				.collect(Collectors.toList());
	}


	/**
	 * @param uploadDocumentDto the {@link com.mycloud.documentservice.dto.UploadDocumentDto} with the upload data
	 * @param result            the result that will be returned to the controller
	 * @param directory         the directory entity which will be associated with the new file
	 * @return a consumer which handles the persisting to database actions when a new file is created
	 */
	private Consumer<SaveFileResult> saveFileConsumer(
			UploadDocumentDto uploadDocumentDto,
			UploadDocumentServiceResult result,
			Directory directory
	) {
		return saveFileResult -> {
			if (saveFileResult.isSaved()) {
				DocumentType documentType = documentManager.determineDocumentFile(
						uploadDocumentDto.getChunk(),
						uploadDocumentDto.getUploadId()
				);

				Document newDocument = new Document.Builder()
						.setDirectory(directory)
						.setName(uploadDocumentDto.getFilename())
						.setUploadedAt(LocalDateTime.now())
						.setType(documentType)
						.setPath(saveFileResult.getPath())
						.setSize(saveFileResult.getSize())
						.build();

				// if the file exists on the given path, which means they have same path/name
				// update the record in db, otherwise create a new one.
				documentRepository.findByPath(saveFileResult.getPath().toString())
						.ifPresentOrElse(document -> {
							document.updateFields(newDocument);
							documentRepository.save(document);
							result.setDocument(DocumentDto.ofDocument(document));
							result.setAlreadyExisted(true);
						}, () -> {
							documentRepository.save(newDocument);
							result.setDocument(DocumentDto.ofDocument(newDocument));
							result.setAlreadyExisted(false);

							filesystemResourceExtraActionService.publishToThirdPartyServices(newDocument, Document.class);
						});


				result.setFinished();


			} else {
				result.setSaved(false);
			}

		};
	}


}
