package com.mycloud.documentservice.services;

import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import com.mycloud.documentservice.elasticdto.UploadItem;
import com.mycloud.documentservice.elasticrepositories.UploadItemRepository;
import com.mycloud.documentservice.entities.Directory;
import com.mycloud.documentservice.entities.Document;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
@Slf4j
public class FilesystemResourceExtraActionServiceImpl implements FilesystemResourceExtraActionService {
	private final RabbitMQPublisherService rabbitMQService;
	private final UploadItemRepository uploadItemRepository;


	@Override
	public <T> void publishToThirdPartyServices(T item, Class<T> cls) {
		if (cls.equals(Document.class)) {
			rabbitMQService.publishNewDocument((Document) item);
			uploadItemRepository.save(UploadItem.ofDocument((Document) item));

			log.info("Added new document to RABBITMQ and ELASTICSEARCH");
		} else if (cls.equals(Directory.class)) {
			rabbitMQService.publishNewDirectory((Directory) item);
			uploadItemRepository.save(UploadItem.ofDirectory((Directory) item));

			log.info("Added new directory to RABBITMQ and ELASTICSEARCH");
		}

	}

	@Override
	public List<ElasticsearchResultDto<UploadItem>> searchInElastic(String name) {
		return uploadItemRepository.searchByName(name);
	}
}
