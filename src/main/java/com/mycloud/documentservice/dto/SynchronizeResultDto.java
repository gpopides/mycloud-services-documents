package com.mycloud.documentservice.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class SynchronizeResultDto {
	private List<UUID> documentIds;
	private List<UUID> directoryIds;
}
