package com.mycloud.documentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LatestDocumentsFilter {
	private int limit;
	private int userID;
}
