package com.mycloud.documentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ElasticsearchResultDto<T> {
	private T data;

}
