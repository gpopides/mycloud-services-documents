package com.mycloud.documentservice.dto.factory;

import com.mycloud.documentservice.dto.DirectoryDto;
import com.mycloud.documentservice.entities.Directory;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DirectoryFactory {
	public static DirectoryDto directoryDto(Directory directory) {
		List<DirectoryDto> subDirectories = Collections.emptyList();

		if (!directory.getSubDirectories().isEmpty()) {
			subDirectories = directory
					.getSubDirectories()
					.stream()
					.map(DirectoryFactory::directoryDto)
					.collect(Collectors.toList());
		}

		return new DirectoryDto(
				directory.getId(),
				directory.getName(),
				directory.normalizePath(),
				subDirectories
		);
	}
}
