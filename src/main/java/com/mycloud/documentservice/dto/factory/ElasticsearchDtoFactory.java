package com.mycloud.documentservice.dto.factory;

import com.mycloud.documentservice.dto.ElasticsearchResultDto;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.List;
import java.util.stream.Collectors;

public class ElasticsearchDtoFactory {
	public static <T> List<ElasticsearchResultDto<T>> buildList(SearchHits<T> hits) {
		return hits.get()
				.map(hit -> new ElasticsearchResultDto<>(hit.getContent()))
				.collect(Collectors.toList());
	}
}
