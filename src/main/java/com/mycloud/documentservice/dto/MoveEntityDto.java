package com.mycloud.documentservice.dto;

import com.mycloud.documentservice.enums.EntityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoveEntityDto {
	private UUID id;
	private EntityType entityType;
}
