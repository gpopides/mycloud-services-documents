package com.mycloud.documentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class DirectoryDto {
	private UUID id;
	private String name;
	private String path;
	private List<DirectoryDto> subdirectories;
}