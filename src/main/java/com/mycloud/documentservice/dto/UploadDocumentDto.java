package com.mycloud.documentservice.dto;

import com.mycloud.documentservice.utils.GeneralUtils;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class UploadDocumentDto {
	@NotNull
	private MultipartFile chunk;
	@NotNull
	private UUID directoryId;
	@NotNull
	private String uploadId;
	@NotNull
	private String chunkId;
	@NotNull
	private String filename;


	public int chunkIdSequence() {
		return GeneralUtils.getFileChunkSequenceIndex(chunkId);
	}

	public String getUploadIdOfChunkId() {
		return GeneralUtils.getChunkIdWithoutSequence(chunkId);
	}

}

