package com.mycloud.documentservice.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
public class UploadDocumentInformationDto {
	@NotNull
	long filesize;
	@NotNull
	UUID directoryId;
	@NotNull
	String filename;
}
