package com.mycloud.documentservice.dto;

import com.mycloud.documentservice.entities.Document;
import com.mycloud.documentservice.enums.DocumentType;
import com.mycloud.documentservice.services.DocumentManager;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
public class DocumentDto {
	private UUID id;
	private String name;
	private DocumentType documentType;
	private boolean canBeSentThroughEmail;

	private DocumentDto(Document document) {
		this.id = document.getId();
		this.name = document.getName();
		this.documentType = document.getDocumentType();
		this.canBeSentThroughEmail = canBeSentThroughEmail(document);
	}

	public static DocumentDto ofDocument(Document document) {
		return new DocumentDto(document);
	}

	/**
	 * @param document the document entity
	 * @return if the extension of the document is a valid extension that can be sent through an email
	 */
	public static boolean canBeSentThroughEmail(Document document) {
		Set<String> validExtensionsForEmail = Set.of("txt");
		return validExtensionsForEmail.contains(DocumentManager.extension(document.getName()));
	}
}
