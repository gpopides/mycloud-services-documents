package com.mycloud.documentservice.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class MoveEntitiesRequest {
	private UUID destinationDirectoryId;
	private List<MoveEntityDto> items;
}
