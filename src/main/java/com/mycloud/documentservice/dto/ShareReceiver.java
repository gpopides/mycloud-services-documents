package com.mycloud.documentservice.dto;

import lombok.Data;

import java.util.Optional;

@Data
public class ShareReceiver {
	private String name;
	private String email;
	private String phone;

	/**
	 * @param method Which method to use for sending a document
	 * @return if the receiver credentials/info are valid based on the send method
	 */
	public boolean receiverInfoIsValid(SendMethod method) {
		boolean valid;

		if (method == SendMethod.EMAIL) {
			valid = Optional.ofNullable(email).isPresent();
		} else if (method == SendMethod.PHONE) {
			valid = Optional.ofNullable(phone).isPresent();
		} else {
			valid = false;
		}

		return valid;
	}
}
