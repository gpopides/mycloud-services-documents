package com.mycloud.documentservice.dto;

public enum SendMethod {
	EMAIL,
	PHONE
}
