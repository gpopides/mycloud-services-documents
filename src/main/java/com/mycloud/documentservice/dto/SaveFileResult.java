package com.mycloud.documentservice.dto;

import lombok.Data;

import java.nio.file.Path;

@Data
public class SaveFileResult {
	private boolean saved;
	private Path path;
	private long size;

}
