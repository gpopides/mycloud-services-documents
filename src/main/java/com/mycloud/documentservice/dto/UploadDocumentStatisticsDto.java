package com.mycloud.documentservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class UploadDocumentStatisticsDto implements Serializable {
	private long chunksCount;
	private String uploadId;
	private long chunkSize;
	private String filename;
	private long totalSize;
}
