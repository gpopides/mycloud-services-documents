package com.mycloud.documentservice.dto;

import com.mycloud.documentservice.dto.factory.DirectoryFactory;
import com.mycloud.documentservice.entities.Directory;
import lombok.Data;


@Data
public class FilesystemTree {
	private DirectoryDto fs;

	public void setFs(Directory rootDirectory) {
		this.fs = DirectoryFactory.directoryDto(rootDirectory);
	}
}
