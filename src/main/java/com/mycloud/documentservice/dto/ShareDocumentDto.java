package com.mycloud.documentservice.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ShareDocumentDto {
	private ShareReceiver receiver;
	private SendMethod method;
	private UUID documentId;
}
