package com.mycloud.documentservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExportDirectoryResult {
	private boolean started;
	private String error;
}
