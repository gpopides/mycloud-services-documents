create table users
(
    id       uuid primary key,
    username varchar(50) unique not null,
    email    varchar(70) unique not null
);



create table directories
(
    id        uuid primary key,
    name      varchar(100) not null,
    path      varchar(200) not null,
    user_id   uuid,
    parent_id uuid,

    constraint user_fk foreign key (user_id) references users (id),
    constraint parent_fk foreign key (parent_id) references directories (id)
);


create table documents
(
    id            uuid primary key,
    document_type varchar(20),
    name          varchar(100) not null,
    uploaded_at   date         not null,
    directory_id  uuid,
    path          varchar(200) not null,
    size          int4,

    constraint directory_fk foreign key (directory_id) references directories (id)
);
