alter table directories
    add column uploaded_at timestamp;

alter table documents
    alter column uploaded_at type timestamp;