alter table directories
    drop constraint if exists user_fk cascade;

drop table if exists users cascade;

alter table directories
    drop column user_id;
alter table directories
    add column user_id int8;
