# mycloud - Document Service

This repository has the code of the Document Service of my cloud.  It is responsible for manipulating document and directories of the **mycloud** app

### Requirements
- java >= 11
- postgres >= 12
- rabbitMQ >= 3.8
- gradle (wrapper, already exists after clone)

Before starting the application, modify the *application.dev-properties* file accordingly.
Then, create a .env file with the placeholders existing in the  *application.dev-properties* file.
If using IntellijIDEA, use the [EnvFile](https://plugins.jetbrains.com/plugin/7861-envfile) plugin. Otherwise, 
``` bash 
cd $PROJECT_DIRECTORY
```
and run 
```bash
export $(cat .env | xargs)
```

## IMPORTANT
Every action with gradle, must be done after having set the environment variables.
So be sure that you have run `export $(cat .env | xargs)` if you are using gradle from a terminal

### Running 
```bash
./gradlew bootRun --args='--spring.profiles.active=dev'
```

### Tests

```bash
./gradlew clean test
```
